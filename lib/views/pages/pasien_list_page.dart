import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/states/pasien_action.dart';
import 'package:ssimu/views/pages/app_pre_page.dart';
import 'package:ssimu/views/widgets/card_pasien.dart';

class PasienListPage extends StatefulWidget {
  @override
  _PasienListPageState createState() => _PasienListPageState();
}

class _PasienListPageState extends State<PasienListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Patient List", style: TextStyle(color: Colors.white)),
        backgroundColor: Color(0XFF28a745),
      ),
      body: StoreConnector<AppState, _DaftarPasienViewModel>(onInit: (store) {
        store.dispatch(LoadListPasien());
      }, converter: (store) {
        print("cek");
        if (store.state.pasienState is ListPasienNotLoaded ||
            store.state.pasienState is ListPasienLoading) {
          print("cek");
          return _DaftarPasienViewModel(isLoading: true, listPasien: null);
        }
        if (store.state.pasienState is ListPasienLoaded) {
          print("cek");
          return _DaftarPasienViewModel(
              isLoading: false,
              listPasien:
                  (store.state.pasienState as ListPasienLoaded).listWeight);
        }
        print("cek");
        return _DaftarPasienViewModel(isLoading: true, listPasien: null);
      }, builder: (context, listPasienVm) {
        print("cek");
        return listPasienVm.isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: listPasienVm.listPasien.length,
                itemBuilder: (BuildContext context, int index) {
                  return (listPasienVm.listPasien[index].weight == null)?
                  Card(
                    elevation: 5,
                    margin: EdgeInsets.all(8),
                    child: ListTile(
                      leading: Icon(
                        Icons.account_circle,
                        color: Colors.green,
                        size: 40,
                      ),
                      title: Text(
                        "${listPasienVm.listPasien[index].nama}",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: Text(
                        listPasienVm.listPasien[index].nomorUrut,
                        style: TextStyle(color: Colors.black),
                      ),
                      trailing: Icon(Icons.edit),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                              print("cek");
                          return AppPrePage(
                            pasien: listPasienVm.listPasien[index],
                            
                          );
                          
                        }));
                      },
                    ),
                  ):
                  Container();
                },
              );
      }),
    );
  }
}

class _DaftarPasienViewModel {
  final bool isLoading;
  final List<Pasien> listPasien;

  _DaftarPasienViewModel({this.isLoading, this.listPasien});
}
