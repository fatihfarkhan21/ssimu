import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:ssimu/action/user_action.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/states/auth_state.dart';
import 'package:ssimu/views/pages/login_ui.dart';

import '../views.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  BuildContext localcontext;
  var store;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    localcontext = context;
    store = StoreProvider.of<AppState>(context);
    return StoreBuilder<AppState>(
      onInit: (store) => store.dispatch(AuthInit()),
      builder: (context, store) {
        if (store.state.authState is AuthenticationLoading) {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (store.state.authState is AuthenticationUnauthenticated) {
          return LoginUi();
        } else {
          return AppPage();
        }
      },
    );
  }
}

// class LoginPage extends StatefulWidget {
//   @override
//   _LoginPageState createState() => _LoginPageState();
// }

// class _LoginPageState extends State<LoginPage> {
//   final _formKey = GlobalKey<FormState>();
//   String email;
//   String password;
//   bool remember = false;
//   final List<String> errors = [];

//   void addError({String error}) {
//     if (!errors.contains(error))
//       setState(() {
//         errors.add(error);
//       });
//   }

//   void removeError({String error}) {
//     if (errors.contains(error))
//       setState(() {
//         errors.remove(error);
//       });
//   }

//   @override
//   Widget build(BuildContext context) {
//     SizeConfig().init(context);
//     return Scaffold(
//         appBar: AppBar(
//           title: Text("Sign In"),
//         ),
//         body: Stack(
//           children: [
//             Container(),
//             SafeArea(
//               child: Container(
//                 width: double.infinity,
//                 child: SingleChildScrollView(
//                   child: Padding(
//                     padding: EdgeInsets.symmetric(
//                         horizontal: getProportionateScreenWidth(20)),
//                     child: Column(
//                       children: [
//                         SizedBox(height: SizeConfig.screenHeight * 0.04),
//                         Text(
//                           "SSIMU",
//                           style: TextStyle(
//                             color: Colors.black,
//                             fontSize: getProportionateScreenWidth(28),
//                             fontWeight: FontWeight.bold,
//                           ),
//                         ),
//                         Text(
//                           "Surgical Site Infection Muhammadiyah",
//                           textAlign: TextAlign.center,
//                         ),
//                         SizedBox(height: SizeConfig.screenHeight * 0.08),
//                         Form(
//                           key: _formKey,
//                           child: Column(
//                             children: [
//                               buildEmailFormField(),
//                               SizedBox(
//                                   height: getProportionateScreenHeight(30)),
//                               buildPasswordFormField(),
//                               SizedBox(
//                                   height: getProportionateScreenHeight(30)),
//                               DefaultButton(
//                                 text: "Continue",
//                                 press: () {
//                                   if (_formKey.currentState.validate()) {
//                                     _formKey.currentState.save();
//                                   }
//                                 },
//                               ),
//                             ],
//                           ),
//                         ),
//                         SizedBox(height: SizeConfig.screenHeight * 0.04),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: [
//                             Text(
//                               "Don't have an account ? ",
//                               style: TextStyle(
//                                   fontSize: getProportionateScreenWidth(16)),
//                             ),
//                             GestureDetector(
//                               onTap: () {
//                                 //Navigator.pushNamed(context, '/signup');
//                               },
//                               child: Text(
//                                 "Sign Up",
//                                 style: TextStyle(
//                                     fontSize: getProportionateScreenWidth(16),
//                                     color: kPrimaryColor),
//                               ),
//                             ),
//                           ],
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Container(),
//                             Image.asset(
//                               "assets/images/login.png",
//                               fit: BoxFit.fitHeight,
//                               scale: 3,
//                             ),
//                           ],
//                         )
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ));
//   }

//   TextFormField buildPasswordFormField() {
//     return TextFormField(
//       obscureText: true,
//       onSaved: (newValue) => password = newValue,
//       onChanged: (value) {
//         if (value.isNotEmpty) {
//           removeError(error: kPassNullError);
//         } else if (value.length >= 8) {
//           removeError(error: kShortPassError);
//         }
//         return null;
//       },
//       validator: (value) {
//         if (value.isEmpty) {
//           addError(error: kPassNullError);
//           return "";
//         } else if (value.length < 8) {
//           addError(error: kShortPassError);
//           return "";
//         }
//         return null;
//       },
//       decoration: InputDecoration(
//         labelText: "Password",
//         hintText: "Enter your password",
//         // If  you are using latest version of flutter then lable text and hint text shown like this
//         // if you r using flutter less then 1.20.* then maybe this is not working properly
//         floatingLabelBehavior: FloatingLabelBehavior.always,
//         //suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
//       ),
//     );
//   }

//   TextFormField buildEmailFormField() {
//     return TextFormField(
//       keyboardType: TextInputType.emailAddress,
//       onSaved: (newValue) => email = newValue,
//       onChanged: (value) {
//         if (value.isNotEmpty) {
//           removeError(error: kEmailNullError);
//         } else if (emailValidatorRegExp.hasMatch(value)) {
//           removeError(error: kInvalidEmailError);
//         }
//         return null;
//       },
//       validator: (value) {
//         if (value.isEmpty) {
//           addError(error: kEmailNullError);
//           return "";
//         } else if (!emailValidatorRegExp.hasMatch(value)) {
//           addError(error: kInvalidEmailError);
//           return "";
//         }
//         return null;
//       },
//       decoration: InputDecoration(
//         labelText: "Email",
//         hintText: "Enter your email",
//         // If  you are using latest version of flutter then lable text and hint text shown like this
//         // if you r using flutter less then 1.20.* then maybe this is not working properly
//         floatingLabelBehavior: FloatingLabelBehavior.always,
//         //suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
//       ),
//     );
//   }
// }
