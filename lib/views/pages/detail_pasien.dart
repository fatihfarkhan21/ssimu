import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/model/surveillance.dart';
import 'package:ssimu/views/widgets/surveillance_page.dart';

import '../views.dart';

class DetailPage extends StatefulWidget {
  final Pasien pasien;

  const DetailPage({Key key, this.pasien}) : super(key: key);
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Pasien p;

  @override
  void initState() {
    p = widget.pasien;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Patient", style: TextStyle(color: Colors.white)),
        backgroundColor: Color(0XFF28a745),
      ),
      body: SafeArea(
          child: Stack(
        children: [
          CustomScrollView(
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: Container(
                  padding: const EdgeInsets.only(left: 8, right: 8),
                  color: Color(0XFF28a745),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                          width: 300,
                          child: Text("${p.nama}" ?? "-",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold))),
                      SizedBox(
                          width: 300,
                          child: Text("${p.diagnosa}" ?? "-",
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16))),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Date of Birth :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.tanggalLahir}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                              SizedBox(height: 10),
                              Text("Surgical Procedure :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.prosedurOperasi}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                              SizedBox(height: 10),
                              Text("Grade :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.grade}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16))
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 2,
                                height: 25,
                                color: Colors.white,
                              ),
                              SizedBox(height: 10),
                              Container(
                                width: 2,
                                height: 25,
                                color: Colors.white,
                              ),
                              SizedBox(height: 10),
                              Container(
                                width: 2,
                                height: 25,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Sex :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.jenisKelamin} " ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                              SizedBox(height: 10),
                              Text("Date of surgery :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.tanggalOperasi}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                              SizedBox(height: 10),
                              Text("Weight :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.weight}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16))
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 2,
                                height: 25,
                                color: Colors.white,
                              ),
                              SizedBox(height: 10),
                              Container(
                                width: 2,
                                height: 25,
                                color: Colors.white,
                              ),
                              SizedBox(height: 10),
                              Container(
                                width: 2,
                                height: 25,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("InPatient Number :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.nomorUrut}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                              SizedBox(height: 10),
                              Text("Operating theater :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.ruangan}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                              SizedBox(height: 10),
                              Text("Height :",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12)),
                              Text("${p.height}" ?? "-",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16))
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
              (p.surveillance != null)
                  ? SliverToBoxAdapter(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Surgical site infection surveillance"),
                            FlatButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              SurveillancePage(p: p)));
                                },
                                child: Text("All...",
                                    style: TextStyle(color: Colors.blue)))
                          ],
                        ),
                      ),
                    )
                  : SliverToBoxAdapter(),
              (p.surveillance != null)
                  ? SliverToBoxAdapter(
                      child: Container(
                        height: 320,
                        child: ListView.builder(
                          itemCount: p.surveillance.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) {
                            var no = index + 1;
                            DateTime tempDate = DateTime.parse(
                                p.surveillance[index].tanggalOperasi);
                            print("cek");
                            return Card(
                              elevation: 5,
                              child: Container(
                                height: 50,
                                width: 300,
                                child: Column(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8),
                                      //margin: EdgeInsets.symmetric(horizontal: 8),
                                      height: 40,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          color: Color(0XFF28a745),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(5),
                                              topRight: Radius.circular(5))),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text("NO " + no.toString(),
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16)),
                                          DateCardRow(
                                            tanggal: tempDate,
                                            colors: Colors.white,
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text("Event :",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12)),
                                            Text(
                                                "${p.surveillance[index].event ?? "-"}",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16)),
                                            Divider(color: Colors.black),
                                            Text("Antibiotic :",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12)),
                                            Text(
                                                "${p.surveillance[index].antibiotik ?? "-"}",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16)),
                                            Divider(color: Colors.black),
                                            Text("symptoms :",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12)),
                                            Text(
                                                "${p.surveillance[index].gejala ?? "-"}",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16)),
                                            Divider(color: Colors.black),
                                            Text("symptoms Details:",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12)),
                                            Text(
                                                "${p.surveillance[index].gejalaDetail ?? "-"} ",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16)),
                                            Divider(color: Colors.black),
                                            Text("surveillance:",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12)),
                                            Text(
                                                "${p.surveillance[index].analis ?? "-"} ",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16)),
                                          ]),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  : SliverToBoxAdapter(),
              SliverToBoxAdapter(
                  child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        //margin: EdgeInsets.symmetric(horizontal: 8),
                        height: 30,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Color(0XFF28a745),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(5),
                                topRight: Radius.circular(5))),
                        child: Center(
                          child: Text("NNIS Risk Index Variables",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                        )),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("ASA Class :",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12)),
                              Text("${p.asaClass ?? "-"}",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16))
                            ],
                          ),
                          Divider(color: Colors.black),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Surgical wound class :",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12)),
                              Text("${p.woundClass ?? "-"}",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16))
                            ],
                          ),
                          Divider(color: Colors.black),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Start time (knife to skin) :",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12)),
                              Text("${p.startSkinKnife ?? "-"} WIB",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16)),
                              Text("End time (skin closure) :",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12)),
                              Text("${p.endSkinKnife ?? "-"} WIB",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16))
                            ],
                          ),
                          Divider(color: Colors.black),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("Urgency of operation :",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12)),
                              Text("${p.urgency ?? "-"}",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16))
                            ],
                          ),
                          Divider(color: Colors.black),
                        ],
                      ),
                    ),
                  ],
                ),
              )),
              SliverToBoxAdapter(
                child: Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          //margin: EdgeInsets.symmetric(horizontal: 8),
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0XFF28a745),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          child: Center(
                            child: Text("Patient preparation",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Shower (full Body) :",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                                Text("${p.shower ?? "-"}",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16))
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Antimicrobial soap :",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                                Text("${p.antiMicroba ?? "-"}",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16))
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Plain soap :",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                                Text("${p.plain ?? "-"}",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16))
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Divider(color: Colors.black),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Hair removal (HR) :",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                                Text("${p.cukur ?? "-"}",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16))
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("HR Place :",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12)),
                                Text("${p.theater ?? "-"}",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 16))
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Card(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          //margin: EdgeInsets.symmetric(horizontal: 8),
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0XFF28a745),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          child: Center(
                            child: Text("Surgical antibiotic prophylaxis",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("prophylaxis required :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.prophylaxis ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16))
                                ]),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Antibiotic given :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.antiBiotik ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16))
                                ]),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Divider(color: Colors.black),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Time given :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.jamStartAntibotik ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16))
                                ]),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Time re-dosed :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.redosedAntibiotik ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16))
                                ]),
                          ],
                        ),
                      )
                    ])),
              ),
              SliverToBoxAdapter(
                child: Card(
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          //margin: EdgeInsets.symmetric(horizontal: 8),
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0XFF28a745),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          child: Center(
                            child: Text("Postoperative antibiotics",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      "Antibiotics ceased at completion of surgery? :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.antibiotikCeased ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                  Text("Reason given :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.reasonGiven ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16))
                                ]),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Card(
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          //margin: EdgeInsets.symmetric(horizontal: 8),
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0XFF28a745),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          child: Center(
                            child: Text("Surgical skin preparation",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Surgical skin preparation :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.surgicalSkin ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                  Text(
                                      "Appropriate skin preparation technique :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.teknikTepat ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                  Text("Allowed to fully dry :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.fullyDry ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16))
                                ]),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Card(
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          //margin: EdgeInsets.symmetric(horizontal: 8),
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0XFF28a745),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          child: Center(
                            child: Text("Surgical hand preparation",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Surgical hand preparation :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.surgicalHand ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                ]),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Card(
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          //margin: EdgeInsets.symmetric(horizontal: 8),
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0XFF28a745),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          child: Center(
                            child: Text("Theatre traffic",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Headcount at start of operation.n :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.jumlahKaryawanAwal ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                  Text("Number of entries during operation :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.jumlahEntri ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                  Text("Door openings during operation :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.jumlahOpenDoor ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                ]),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Card(
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 8),
                          //margin: EdgeInsets.symmetric(horizontal: 8),
                          height: 30,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Color(0XFF28a745),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(5),
                                  topRight: Radius.circular(5))),
                          child: Center(
                            child: Text("Drain / implant",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                          )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Location :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.locationImplant ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                  Text("Drain inserted :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.typeDrain ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                  Text("Implant used :",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text("${p.typeImplant ?? "-"}",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16)),
                                  SizedBox(height: 10),
                                ]),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          )
        ],
      )),
    );
  }
}
