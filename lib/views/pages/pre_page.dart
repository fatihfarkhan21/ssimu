import 'package:after_init/after_init.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:ssimu/model/pasien.dart';

import '../views.dart';

class PrePage extends StatefulWidget {
  final Function(int) jumpToPage;
  final ValueSetter<Pasien> onEdit;

  final Pasien pasien;

  const PrePage({Key key, this.jumpToPage, this.onEdit, this.pasien})
      : super(key: key);
  @override
  _PrePageState createState() => _PrePageState();
}

class _PrePageState extends State<PrePage>
    with AfterInitMixin<PrePage>, AutomaticKeepAliveClientMixin<PrePage> {
  @override
  void didInitState() {}

  Pasien p;
  @override
  void initState() {
    if (widget.pasien != null) {
      p = widget.pasien;
      tinggi = p.height;
      p.weight = "test";
    }

    print("cek");
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;
  final _formKey = GlobalKey<FormState>();

  final GlobalKey<ExpansionTileCardState> cardC = new GlobalKey();
  String berat;
  String tinggi;
  String total;
  String _timeStart = "Not set";
  String _timeEnd = "Not set";

  String valueAsa;
  String valueWound;
  String valueUrgency;

  List urgencyList = [
    'Emergency – must be done immediately to save life',
    'Urgent – must be done within 24-48h',
    'Semi-elective – must be done within days-weeks',
    'Elective – no time constraints'
  ];

  

  List asaClass = [
    "Normal healthy person",
    "Mild systemic disease (e.g. hypertension, well controlled diabetes)",
    "Severe systemic disease not incapacitating (e.g. moderate COPD, diabetes, malignancy)",
    "Incapacitating systemic disease that is a constant threat to life (e.g. pre-eclampsia, heavy bleeding)",
    "Moribund patient, not expected to survive with or without operation (e.g. major trauma)"
  ];

  List woundClass = [
    "Sterile tissue with no resident bacteria e.g. neurosurgery",
    "CONTROLLED entry to tissue with resident bacteria e.g. hysterectomy",
    "UNCONTROLLED entry to tissue with bacteria e.g. acute gastrointestinal perforation",
    "Heavy contamination (e.g. soil in wound) or infection already established"
  ];
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter: ColorFilter.mode(
                        Colors.white.withOpacity(0.2), BlendMode.dstATop),
                    image: AssetImage("assets/images/background.png"))),
          ),
          CustomScrollView(slivers: <Widget>[
            SliverToBoxAdapter(
                child: Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                    height: 20,
                    width: double.infinity,
                    color: Colors.black54,
                    child: Center(
                      child: Text(
                        "Patient Data",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                    buildBeratFormField(),
                    SizedBox(
                      height: 20,
                    ),
                    buildTinggiFormField(),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(5)),
                      child: DropdownButton(
                        hint: Text(" ASA Class "),
                        icon: Icon(Icons.arrow_drop_down),
                        isExpanded: true,
                        underline: SizedBox(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                        ),
                        value: valueAsa,
                        onChanged: (newValue) {
                          setState(() {
                            valueAsa = newValue;
                          });
                        },
                        items: asaClass.map((value) {
                          return DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(5)),
                      child: DropdownButton(
                        hint: Text("Surgical wound class"),
                        icon: Icon(Icons.arrow_drop_down),
                        isExpanded: true,
                        underline: SizedBox(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                        ),
                        value: valueWound,
                        onChanged: (newValue) {
                          setState(() {
                            valueWound = newValue;
                          });
                        },
                        items: woundClass.map((value) {
                          return DropdownMenuItem(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 8),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.circular(5)),
                      child: DropdownButton(
                        hint: Text("Urgency of operation"),
                        icon: Icon(Icons.arrow_drop_down),
                        isExpanded: true,
                        underline: SizedBox(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                        ),
                        value: valueUrgency,
                        onChanged: (newValue) {
                          setState(() {
                            valueUrgency = newValue;
                          });
                        },
                        items: urgencyList.map((valueItem) {
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(valueItem),
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            )),
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text("Start",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                        )),
                  ),
                  InkWell(
                    onTap: () {
                      DatePicker.showTimePicker(context,
                          theme: DatePickerTheme(
                            containerHeight: 210.0,
                          ),
                          showTitleActions: true, onConfirm: (time) {
                        print('confirm $time');

                        _timeStart =
                            '${time.hour} : ${time.minute} : ${time.second}';

                        setState(() {});
                      }, currentTime: DateTime.now(), locale: LocaleType.en);

                      setState(() {});
                    },
                    child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      height: 50.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.access_time,
                                      size: 18.0,
                                      color: Colors.teal,
                                    ),
                                    Text(
                                      " $_timeStart",
                                      style: TextStyle(
                                          color: Colors.teal,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          Text(
                            "  Change",
                            style: TextStyle(
                                color: Colors.teal,
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SliverToBoxAdapter(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Divider(color: Colors.black),
            )),
            SliverToBoxAdapter(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text("End",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      )),
                ),
                InkWell(
                  onTap: () {
                    DatePicker.showTimePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 210.0,
                        ),
                        showTitleActions: true, onConfirm: (time) {
                      print('confirm $time');

                      _timeEnd =
                          '${time.hour} : ${time.minute} : ${time.second}';

                      setState(() {});
                    }, currentTime: DateTime.now(), locale: LocaleType.en);

                    setState(() {});
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    margin: EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.access_time,
                                    size: 18.0,
                                    color: Colors.teal,
                                  ),
                                  Text(
                                    " $_timeEnd",
                                    style: TextStyle(
                                        color: Colors.teal,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Text(
                          "  Change",
                          style: TextStyle(
                              color: Colors.teal,
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: DefaultButton(
                    text: "Continue",
                    press: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();

                        widget.pasien.height = tinggi;
                        widget.pasien.weight = berat;
                        widget.pasien.asaClass = valueAsa;
                        widget.pasien.woundClass = valueWound;
                        widget.pasien.urgency = valueUrgency;
                        widget.pasien.startSkinKnife = _timeStart;
                        widget.pasien.endSkinKnife = _timeEnd;
                        print("cek");
                      }
                      widget.onEdit(p);

                      print("cek");
                    }),
              ),
            )
          ])
        ],
      )),
    );
  }

  TextFormField buildBeratFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => berat = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Weight",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  TextFormField buildTinggiFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => tinggi = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Height",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }
}
