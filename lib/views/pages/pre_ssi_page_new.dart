import 'package:flutter/material.dart';

import '../views.dart';

class PreSsiPageNewPage extends StatefulWidget {
  @override
  _PreSsiPageNewPageState createState() => _PreSsiPageNewPageState();
}

class _PreSsiPageNewPageState extends State<PreSsiPageNewPage> {
  PageController _pageController;
  int _currentPage = 0;
  @override
  void initState() {
    super.initState();
    _pageController = PageController(keepPage: true);
  }

  @override
  Widget build(BuildContext context) {

    var pages = <Widget>[
      UserFormPage(
        jumpToPage: _jumpToPage,
      ),
      LoginPage()
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text("Surgical site infection surveillance"),
        elevation: 0,
      ),
      body: PageView(
        children: pages,
        controller: _pageController,
        onPageChanged: (value){
          setState(() {
            _currentPage = value;
          });
        },
      ),
    );

    
  }
  _jumpToPage(int page) {
    _pageController.animateToPage(page, duration: Duration(milliseconds: 300), curve: Curves.linear);
  }
   @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
}
