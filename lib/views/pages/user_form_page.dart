import 'package:flutter/material.dart';
import 'package:after_init/after_init.dart';
import '../views.dart';

class UserFormPage extends StatefulWidget {
  final Function(int) jumpToPage;
  const UserFormPage({
    Key key,
    this.jumpToPage,
  }) : super(key: key);

  @override
  _UserFormPageState createState() => _UserFormPageState();
}

class _UserFormPageState extends State<UserFormPage>
    with
        AfterInitMixin<UserFormPage>,
        AutomaticKeepAliveClientMixin<UserFormPage> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Patient Form", style: TextStyle(color: Colors.white)),
        backgroundColor: Color(0XFF28a745),
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter: new ColorFilter.mode(
                        Colors.white.withOpacity(0.2), BlendMode.dstATop),
                    image: AssetImage("assets/images/background.png"))),
          ),
          SingleChildScrollView(
              child: Column(
            children: [
              Container(
                  margin: EdgeInsets.all(8),
                  height: 35,
                  width: double.infinity,
                  padding: EdgeInsets.all(8),
                  color: Colors.black54,
                  child: Text("Patient Data",
                      style: TextStyle(color: Colors.white))),
              FormUser(),

              // RaisedButton(onPressed: (){
              //   widget.jumpToPage(1);
              // })
            ],
          )),
        ],
      ),
    );
  }

  @override
  void didInitState() {}

  @override
  bool get wantKeepAlive => true;
}
