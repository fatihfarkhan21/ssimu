import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:ssimu/views/widgets/date_card_row.dart';

import '../views.dart';

class PreSsiPage extends StatefulWidget {
  @override
  _PreSsiPageState createState() => _PreSsiPageState();
}

class _PreSsiPageState extends State<PreSsiPage> {
  final GlobalKey<ExpansionTileCardState> cardA = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardB = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardC = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardD = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardE = new GlobalKey();
  final GlobalKey<ExpansionTileCardState> cardF = new GlobalKey();

  String name;
  String age;
  String nomorPasien;
  String diagnosa;
  DateTime _tanggalLahir;
  DateTime _dateReg;
  String _timeStart = "Not set";
  String _timeEnd = "Not set";

  @override
  void initState() {
    _tanggalLahir = DateTime.now();
    _dateReg = DateTime.now();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
          title: Text("Pre SSI Form"),
        ),
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        colorFilter: ColorFilter.mode(
                            Colors.white.withOpacity(0.2), BlendMode.dstATop),
                        image: AssetImage("assets/images/background.png"))),
              ),
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Patient Data", style: TextStyle(fontSize: 16)),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildNameFormField(),
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildNomorPasienFormField(),
                                Container(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  height: 50,
                                  child: InkWell(
                                    onTap: () async {
                                      DateTime picked = await showDatePicker(
                                          context: context,
                                          initialDate: _dateReg,
                                          firstDate: DateTime(2020),
                                          lastDate: _dateReg);

                                      if (picked != null &&
                                          picked != _dateReg) {
                                        setState(() {
                                          _dateReg =
                                              picked.add(Duration(hours: 17));
                                        });
                                      }
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Admission Date"),
                                        DateCardRow(tanggal: _dateReg),
                                        Icon(Icons.calendar_today)
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildDiagnosaFormField(),
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                Container(
                                  padding: const EdgeInsets.only(right: 16),
                                  height: 50,
                                  child: Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Text("Age"),
                                            Container(
                                              height: 30,

                                              width: 30,

                                              margin: EdgeInsets.only(
                                                  left: 5, right: 5),

                                              //color: Colors.blue,

                                              child: buildAgeFormField(),
                                            ),
                                            Text("Th")
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text("Sex : "),
                                            Container(
                                              width: 75,
                                              child: CheckboxListTile(
                                                value: true,
                                                title: Text("F"),
                                                controlAffinity:
                                                    ListTileControlAffinity
                                                        .leading,
                                                onChanged: (value) {
                                                  // setState(() {

                                                  //   _menuItem.isCatering = value;

                                                  // });
                                                },
                                                activeColor: Colors.blue,
                                              ),
                                            ),
                                            Container(
                                              width: 75,
                                              child: CheckboxListTile(
                                                value: false,
                                                title: Text("M"),
                                                controlAffinity:
                                                    ListTileControlAffinity
                                                        .leading,
                                                onChanged: (value) {
                                                  // setState(() {

                                                  //   _menuItem.isCatering = value;

                                                  // });
                                                },
                                                activeColor: Colors.blue,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  height: 50,
                                  child: InkWell(
                                    onTap: () async {
                                      DateTime picked = await showDatePicker(
                                          context: context,
                                          initialDate: _tanggalLahir,
                                          firstDate: DateTime(1888),
                                          lastDate: DateTime.now());

                                      if (picked != null &&
                                          picked != _tanggalLahir) {
                                        setState(() {
                                          _tanggalLahir =
                                              picked.add(Duration(hours: 17));
                                        });
                                      }
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Date Of Birth"),
                                        SizedBox(
                                            width:
                                                SizeConfig.screenWidth * 0.02),
                                        DateCardRow(tanggal: _tanggalLahir),
                                        SizedBox(
                                            width:
                                                SizeConfig.screenWidth * 0.02),
                                        Icon(Icons.calendar_today)
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),

                      SizedBox(height: SizeConfig.screenHeight * 0.02),

                      Text(
                        "Surgical Procedure",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),

                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildProcedurFormField(),
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildGradeFormField(),
                                Container(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  height: 50,
                                  child: InkWell(
                                    onTap: () async {
                                      DateTime picked = await showDatePicker(
                                          context: context,
                                          initialDate: _dateReg,
                                          firstDate: DateTime(2020),
                                          lastDate: _dateReg);

                                      if (picked != null &&
                                          picked != _dateReg) {
                                        setState(() {
                                          _dateReg =
                                              picked.add(Duration(hours: 17));
                                        });
                                      }
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("Date of Surgey"),
                                        DateCardRow(tanggal: _dateReg),
                                        Icon(Icons.calendar_today)
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildRoomSurgicalFormField(),
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildLeadFormField(),
                                Container(
                                  height: 50,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),

                      SizedBox(height: SizeConfig.screenHeight * 0.02),

                      // index risk

                      Text(
                        "Index Risk Variables",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),

                      Row(
                        children: [
                          // Asa class

                          Expanded(
                            child: ExpansionTileCard(
                              initialElevation: 3,
                              key: cardA,
                              baseColor: Colors.white,
                              expandedColor: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              children: [
                                Container(
                                  child: CheckboxListTile(
                                    value: false,
                                    title: Text("Normaly healthy person"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "Mild systemic disease (e.g. hypertension, well controlled diabetes)"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "Severe systemic disease not incapacitating (e.g. moderate COPD, diabetes, malignancy)"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "Incapacitating systemic disease that is a constant threat to life (e.g. pre-eclampsia, heavy bleeding)"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "Moribund patient, not expected to survive with or without operation (e.g. major trauma)"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                              ],
                              title: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text("ASA Class",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ))),
                              elevation: 2,
                            ),
                          ),

                          SizedBox(width: SizeConfig.screenWidth * 0.02),

                          Expanded(
                            child: ExpansionTileCard(
                              initialElevation: 3,
                              key: cardB,
                              baseColor: Colors.white,
                              expandedColor: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              children: [
                                Container(
                                  child: CheckboxListTile(
                                    value: false,
                                    title: Text(
                                        "Sterile tissue with no resident bacteria e.g. neurosurgery"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "CONTROLLED entry to tissue with resident bacteria e.g. hysterectomy"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "UNCONTROLLED entry to tissue with bacteria e.g. acute gastrointestinal perforation"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "Heavy contamination (e.g. soil in wound) or infection already established"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                              ],
                              title: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text("Surgical Wound Class",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ))),
                              elevation: 2,
                            ),
                          ),
                        ],
                      ),

                      SizedBox(height: SizeConfig.screenHeight * 0.02),

                      Row(
                        children: [
                          // knife to skin

                          Expanded(
                            child: ExpansionTileCard(
                              initialElevation: 3,
                              key: cardC,
                              baseColor: Colors.white,
                              expandedColor: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10.0),
                                      child: Text("Start",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600,
                                          )),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        DatePicker.showTimePicker(context,
                                            theme: DatePickerTheme(
                                              containerHeight: 210.0,
                                            ),
                                            showTitleActions: true,
                                            onConfirm: (time) {
                                          print('confirm $time');

                                          _timeStart =
                                              '${time.hour} : ${time.minute} : ${time.second}';

                                          setState(() {});
                                        },
                                            currentTime: DateTime.now(),
                                            locale: LocaleType.en);

                                        setState(() {});
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.all(8),
                                        height: 50.0,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.access_time,
                                                        size: 18.0,
                                                        color: Colors.teal,
                                                      ),
                                                      Text(
                                                        " $_timeStart",
                                                        style: TextStyle(
                                                            color: Colors.teal,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 18.0),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                            Text(
                                              "  Change",
                                              style: TextStyle(
                                                  color: Colors.teal,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18.0),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SizedBox(
                                    height: SizeConfig.screenHeight * 0.02,
                                    child:
                                        Divider(height: 5, color: Colors.black),
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(left: 10.0),
                                      child: Text("End",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600,
                                          )),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        DatePicker.showTimePicker(context,
                                            theme: DatePickerTheme(
                                              containerHeight: 210.0,
                                            ),
                                            showTitleActions: true,
                                            onConfirm: (time) {
                                          print('confirm $time');

                                          _timeEnd =
                                              '${time.hour} : ${time.minute} : ${time.second}';

                                          setState(() {});
                                        },
                                            currentTime: DateTime.now(),
                                            locale: LocaleType.en);

                                        setState(() {});
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        height: 50.0,
                                        margin: EdgeInsets.all(8),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  child: Row(
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.access_time,
                                                        size: 18.0,
                                                        color: Colors.teal,
                                                      ),
                                                      Text(
                                                        " $_timeEnd",
                                                        style: TextStyle(
                                                            color: Colors.teal,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 18.0),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                            Text(
                                              "  Change",
                                              style: TextStyle(
                                                  color: Colors.teal,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18.0),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                              title: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text("Knife to skin",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ))),
                              elevation: 2,
                            ),
                          ),

                          SizedBox(width: SizeConfig.screenWidth * 0.02),

                          Expanded(
                            child: ExpansionTileCard(
                              initialElevation: 3,
                              key: cardD,
                              baseColor: Colors.white,
                              expandedColor: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              children: [
                                Container(
                                  child: CheckboxListTile(
                                    value: false,
                                    title: Text(
                                        "Emergency – must be done immediately to save life"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "Urgent – must be done within 24-48h"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title: Text(
                                        "Semi-elective – must be done within days-weeks"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                                Container(
                                  child: CheckboxListTile(
                                    value: true,
                                    title:
                                        Text("Elective – no time constraints"),
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    onChanged: (value) {
                                      // setState(() {

                                      //   _menuItem.isCatering = value;

                                      // });
                                    },
                                    activeColor: Colors.blue,
                                  ),
                                ),
                              ],
                              title: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text("Urgency of operation",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ))),
                              elevation: 2,
                            ),
                          ),
                        ],
                      ),

                      SizedBox(height: SizeConfig.screenHeight * 0.02),

                      // index risk close

                      Container(
                          height: 30,
                          width: double.infinity,
                          color: Colors.black54,
                          child: Center(
                              child: Text(
                            "PRE/PERI-OPERATIVE PROCESS MEASURES",
                            style: TextStyle(color: Colors.white),
                          ))),

                      SizedBox(height: SizeConfig.screenHeight * 0.02),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: ExpansionTileCard(
                              title: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text("Patient preparation",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ))),
                              elevation: 2,
                              initialElevation: 3,
                              key: cardE,
                              baseColor: Colors.white,
                              expandedColor: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              children: [
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Pre-op bath /shower : "),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            child: CheckboxListTile(
                                              contentPadding: EdgeInsets.all(0),
                                              value: true,
                                              title: Text("YES"),
                                              controlAffinity:
                                                  ListTileControlAffinity
                                                      .leading,
                                              onChanged: (value) {
                                                // setState(() {

                                                //   _menuItem.isCatering = value;

                                                // });
                                              },
                                              activeColor: Colors.blue,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            child: CheckboxListTile(
                                              contentPadding: EdgeInsets.all(0),
                                              value: false,
                                              title: Text("NO"),
                                              controlAffinity:
                                                  ListTileControlAffinity
                                                      .leading,
                                              onChanged: (value) {
                                                // setState(() {

                                                //   _menuItem.isCatering = value;

                                                // });
                                              },
                                              activeColor: Colors.blue,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Antimicrobial soap used : "),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            child: CheckboxListTile(
                                              contentPadding: EdgeInsets.all(0),
                                              value: true,
                                              title: Text("YES"),
                                              controlAffinity:
                                                  ListTileControlAffinity
                                                      .leading,
                                              onChanged: (value) {
                                                // setState(() {

                                                //   _menuItem.isCatering = value;

                                                // });
                                              },
                                              activeColor: Colors.blue,
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                            child: CheckboxListTile(
                                              contentPadding: EdgeInsets.all(0),
                                              value: false,
                                              title: Text("NO"),
                                              controlAffinity:
                                                  ListTileControlAffinity
                                                      .leading,
                                              onChanged: (value) {
                                                // setState(() {

                                                //   _menuItem.isCatering = value;

                                                // });
                                              },
                                              activeColor: Colors.blue,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Hair removal (HR) : "),
                                    Container(
                                      child: CheckboxListTile(
                                        contentPadding: EdgeInsets.all(0),
                                        value: true,
                                        title: Text("Razor"),
                                        controlAffinity:
                                            ListTileControlAffinity.leading,
                                        onChanged: (value) {
                                          // setState(() {

                                          //   _menuItem.isCatering = value;

                                          // });
                                        },
                                        activeColor: Colors.blue,
                                      ),
                                    ),
                                    Container(
                                      child: CheckboxListTile(
                                        contentPadding: EdgeInsets.all(0),
                                        value: false,
                                        title: Text("Clippers"),
                                        controlAffinity:
                                            ListTileControlAffinity.leading,
                                        onChanged: (value) {
                                          // setState(() {

                                          //   _menuItem.isCatering = value;

                                          // });
                                        },
                                        activeColor: Colors.blue,
                                      ),
                                    ),
                                    Container(
                                      child: CheckboxListTile(
                                        contentPadding: EdgeInsets.all(0),
                                        value: false,
                                        title: Text("None"),
                                        controlAffinity:
                                            ListTileControlAffinity.leading,
                                        onChanged: (value) {
                                          // setState(() {

                                          //   _menuItem.isCatering = value;

                                          // });
                                        },
                                        activeColor: Colors.blue,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),

                          SizedBox(width: SizeConfig.screenWidth * 0.02),

                          // antibiotic

                          Expanded(
                            child: ExpansionTileCard(
                              title: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Text("Surgical antibiotic prophylaxis",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ))),
                              elevation: 2,
                              initialElevation: 3,
                              key: cardF,
                              baseColor: Colors.white,
                              expandedColor: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              children: [
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: CheckboxListTile(
                                        contentPadding: EdgeInsets.all(0),
                                        value: true,
                                        title: Text("No prophylaxis required"),
                                        controlAffinity:
                                            ListTileControlAffinity.leading,
                                        onChanged: (value) {
                                          // setState(() {

                                          //   _menuItem.isCatering = value;

                                          // });
                                        },
                                        activeColor: Colors.blue,
                                      ),
                                    ),
                                    Container(
                                      child: CheckboxListTile(
                                        contentPadding: EdgeInsets.all(0),
                                        value: false,
                                        title: Text(
                                            "Required but not given due to :"),
                                        subtitle: Text("Unavailable"),
                                        controlAffinity:
                                            ListTileControlAffinity.leading,
                                        onChanged: (value) {
                                          // setState(() {

                                          //   _menuItem.isCatering = value;

                                          // });
                                        },
                                        activeColor: Colors.blue,
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Text("Other :"),
                                        Container(
                                          height: 30,

                                          width: 160,

                                          margin: EdgeInsets.only(
                                              left: 5, right: 5),

                                          //color: Colors.blue,

                                          child: buildAntibiotikFormField(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Antimicrobial soap used : "),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: true,
                                            contentPadding: EdgeInsets.all(0),
                                            title: Text("Co-amoxiclav"),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: false,
                                            title: Text("Cefazolin"),
                                            contentPadding: EdgeInsets.all(0),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: true,
                                            contentPadding: EdgeInsets.all(0),
                                            title: Text("Cloxacillin"),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: false,
                                            title: Text("Vancomycin"),
                                            contentPadding: EdgeInsets.all(0),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: true,
                                            contentPadding: EdgeInsets.all(0),
                                            title: Text("Ciprofloxacin"),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: false,
                                            title: Text("Gentamicin"),
                                            contentPadding: EdgeInsets.all(0),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: true,
                                            contentPadding: EdgeInsets.all(0),
                                            title: Text("Metronidazole"),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                        Expanded(
                                          child: CheckboxListTile(
                                            value: false,
                                            title: Text("Penicillin"),
                                            contentPadding: EdgeInsets.all(0),
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            onChanged: (value) {
                                              // setState(() {

                                              //   _menuItem.isCatering = value;

                                              // });
                                            },
                                            activeColor: Colors.blue,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      // antibiotik
                      SizedBox(height: SizeConfig.screenHeight * 0.02),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    "Were antibiotics ceased at completion of surgery?"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("YES"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title: Text("NO"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Text(
                                    "If not, what antibiotics were prescribed?"),
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                buildDrugFormField(),
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.02),
                                Text("Dose"),
                                SizedBox(
                                    height: SizeConfig.screenHeight * 0.01),
                                Row(
                                  children: [
                                    Container(
                                        width: 75,
                                        margin: EdgeInsets.only(right: 8),
                                        child: buildDoseFormField()),
                                    Text("Mg")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: SizeConfig.screenWidth * 0.02),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Reason given"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Post-op prophylaxis "),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {},
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title:
                                              Text("Drain / implant inserted"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text(
                                              "Treating suspected / known infection "),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {},
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                buildReasonGivenFormField(),
                                Container(height: 80),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: SizeConfig.screenHeight * 0.02),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    "Surgical skin preparation (under sterile conditions)"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Chlorhex-alc"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title: Text("Iodine+alc "),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Chlorhex-aq"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title: Text("Iodine-aq "),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Text("Appropriate skin preparation technique"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Yes"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title: Text("No "),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Text("Allowed to fully dry"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Yes"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title: Text("No "),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(width: SizeConfig.screenWidth * 0.02),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Surgical hand preparation"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Alcohol-based hand rub"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {},
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title:
                                              Text("Antimicrobial soap+water"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Plain soap+water"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {},
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Text("Appropriate hand preparation technique"),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: true,
                                          title: Text("Yes"),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: CheckboxListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          value: false,
                                          title: Text("No "),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (value) {
                                            // setState(() {

                                            //   _menuItem.isCatering = value;

                                            // });
                                          },
                                          activeColor: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                buildReasonGivenFormField(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  TextFormField buildDoseFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      inputFormatters: [
        LengthLimitingTextInputFormatter(2),
      ],

      onSaved: (newValue) => age = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(

          //labelText: "Pasien Name",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          contentPadding: EdgeInsets.symmetric(horizontal: 5)

          ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
          ),
    );
  }

  TextFormField buildNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Patient Name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildReasonGivenFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Reason Given",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildDrugFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Drug",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildProcedurFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Surgical Procedure",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildRoomSurgicalFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Operating theater",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildLeadFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Lead Surgeon Name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildGradeFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Grade",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildDiagnosaFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => diagnosa = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Primary Diagnosis",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildNomorPasienFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Patient Number",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildNomorPetugasFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Survilance Number",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildAgeFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      inputFormatters: [
        LengthLimitingTextInputFormatter(2),
      ],

      onSaved: (newValue) => age = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(

          //labelText: "Pasien Name",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          contentPadding: EdgeInsets.symmetric(horizontal: 5)

          ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
          ),
    );
  }

  TextFormField buildAntibiotikFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      inputFormatters: [
        LengthLimitingTextInputFormatter(2),
      ],

      onSaved: (newValue) => age = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(

          //labelText: "Pasien Name",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          contentPadding: EdgeInsets.symmetric(horizontal: 5)

          ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
          ),
    );
  }
}
