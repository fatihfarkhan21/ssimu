import 'package:after_init/after_init.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/views/views.dart';
import 'package:ssimu/views/widgets/date_card_row.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class PriPage extends StatefulWidget {
  final Pasien pasien;

  const PriPage({Key key, this.pasien}) : super(key: key);
  @override
  _PriPageState createState() => _PriPageState();
}

class _PriPageState extends State<PriPage>
    with AfterInitMixin<PriPage>, AutomaticKeepAliveClientMixin<PriPage> {
  final _formKey = GlobalKey<FormState>();

  DateTime _dateReg;
  String valueChoose;
  String valueChoose_2;
  String givenchoose;
  String reasonchoose;
  String skinchoose;
  String surgicalchoose;
  String implantchoose;
  String hrdate;
  String headcont;
  String entries;
  String door;
  String location;
  List listimplant = ['Metal (Ortho)', 'Skin graft', 'Mesh', 'Other', 'None'];
  List listItem = ['Razor', 'Clippers', 'None'];
  List listItem_2 = ['Home', 'Ward', 'Theatre'];
  List listgiven = [
    'Co-amoxiclav',
    'Cefazolin',
    'Cloxacillin',
    'Vancomycin',
    'Ciprofloxacin',
    'Gentamicin',
    'Metronidazole',
    'Penicillin'
  ];
  List listreason = [
    'Post-op prophylaxis',
    'Drain / implant inserted',
    'Treating suspected / known infection'
  ];
  List listskin = ['Chlorhex-alc', 'lodine+alc', 'Chlorhex-aq', 'lodine-aq'];
  List listsurgical = [
    'Alcohol-based hand rub',
    'Antimicrobial soap+water',
    'Plain soap+water'
  ];

  List shower = ["Yes", "No"];
  List microba = ["Yes", "No"];
  List plain = ["Yes", "No"];
  List prophylaxis = ["Yes", "No"];
  List antibiotikCeased = ["Yes", "No"];
  List skinPreparation = ["Yes", "No"];
  List fullyDry = ["Yes", "No"];
  List hand = ["Yes", "No"];
  List handPrepare = ["Open", "Closed"];
  List drain = ["Yes", "No"];

  String selectshower;
  String selectmicroba;
  String selectplain;
  String selectprophylaxis;
  String selectantibiotikCeased;
  String selectskinPreparation;
  String selectfullyDry;
  String selecthand;
  String selecthandPrepare;
  String selectdrain;

  String _timeStart = "Not set";
  String _timeEnd = "Not set";
  String _timeProcedure = "Not set";
  var store;

  @override
  void initState() {
    var p = widget.pasien;
    print("cek");
    _dateReg = DateTime.now();
    super.initState();
  }

  @override
  void didInitState() {}

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<AppState>(context);
    super.build(context);
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter: ColorFilter.mode(
                        Colors.white.withOpacity(0.2), BlendMode.dstATop),
                    image: AssetImage("assets/images/background.png"))),
          ),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 20,
                    width: double.infinity,
                    color: Colors.black54,
                    child: Center(
                      child: Text(
                        "Patient preparation",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Pre-op bath/shower (full body)"),
                          Row(
                            children: [
                              showerButton(0, 'Yes'),
                              showerButton(1, "No"),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                height: 30,
                                child: InkWell(
                                  onTap: () async {
                                    DateTime picked = await showDatePicker(
                                        context: context,
                                        initialDate: _dateReg,
                                        firstDate: DateTime(2020),
                                        lastDate: _dateReg);

                                    if (picked != null && picked != _dateReg) {
                                      setState(() {
                                        _dateReg =
                                            picked.add(Duration(hours: 17));
                                      });
                                    }
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("Date"),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      DateCardRow(tanggal: _dateReg),
                                      Icon(Icons.calendar_today)
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Antimicrobial soap used"),
                                Row(
                                  children: [
                                    antimicrobaButton(0, "Yes"),
                                    antimicrobaButton(1, "No"),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Plain soap used"),
                                Row(
                                  children: [
                                    plainButton(0, "Yes"),
                                    plainButton(1, "No")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: DropdownButton(
                                    hint: Text("Hair removal (HR) "),
                                    icon: Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    value: valueChoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        valueChoose = newValue;
                                      });
                                    },
                                    items: listItem.map((valueItem) {
                                      return DropdownMenuItem(
                                        value: valueItem,
                                        child: Text(valueItem),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: DropdownButton(
                                    hint: Text("surgery room"),
                                    icon: Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    value: valueChoose_2,
                                    onChanged: (newValue) {
                                      setState(() {
                                        valueChoose_2 = newValue;
                                      });
                                    },
                                    items: listItem_2.map((valueItem) {
                                      return DropdownMenuItem(
                                        value: valueItem,
                                        child: Text(valueItem),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 20,
                                  width: double.infinity,
                                  color: Colors.black54,
                                  child: Center(
                                    child: Text(
                                      "Surgical antibiotic prophylaxis",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Prophylaxis Required"),
                                Row(
                                  children: [
                                    prophylaxisButton(0, "Yes"),
                                    prophylaxisButton(1, "No")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: DropdownButton(
                                    hint: Text("Antibiotic Given"),
                                    icon: Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    value: givenchoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        givenchoose = newValue;
                                      });
                                    },
                                    items: listgiven.map((valueItem) {
                                      return DropdownMenuItem(
                                        value: valueItem,
                                        child: Text(valueItem),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text("Time Given",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )),
                                ),
                                InkWell(
                                  onTap: () {
                                    DatePicker.showTimePicker(context,
                                        theme: DatePickerTheme(
                                          containerHeight: 210.0,
                                        ),
                                        showTitleActions: true,
                                        onConfirm: (time) {
                                      print('confirm $time');

                                      _timeStart =
                                          '${time.hour} : ${time.minute} : ${time.second}';

                                      setState(() {});
                                    },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en);

                                    setState(() {});
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 50.0,
                                    margin: EdgeInsets.symmetric(horizontal: 8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Icon(
                                                    Icons.access_time,
                                                    size: 18.0,
                                                    color: Colors.teal,
                                                  ),
                                                  Text(
                                                    " $_timeStart",
                                                    style: TextStyle(
                                                        color: Colors.teal,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18.0),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        Text(
                                          "  Change",
                                          style: TextStyle(
                                              color: Colors.teal,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text("Time Re-Dosed",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )),
                                ),
                                InkWell(
                                  onTap: () {
                                    DatePicker.showTimePicker(context,
                                        theme: DatePickerTheme(
                                          containerHeight: 210.0,
                                        ),
                                        showTitleActions: true,
                                        onConfirm: (time) {
                                      print('confirm $time');

                                      _timeEnd =
                                          '${time.hour} : ${time.minute} : ${time.second}';

                                      setState(() {});
                                    },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en);

                                    setState(() {});
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 50.0,
                                    margin: EdgeInsets.symmetric(horizontal: 8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Icon(
                                                    Icons.access_time,
                                                    size: 18.0,
                                                    color: Colors.teal,
                                                  ),
                                                  Text(
                                                    " $_timeEnd",
                                                    style: TextStyle(
                                                        color: Colors.teal,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18.0),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        Text(
                                          "  Change",
                                          style: TextStyle(
                                              color: Colors.teal,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 20,
                                  width: double.infinity,
                                  color: Colors.black54,
                                  child: Center(
                                    child: Text(
                                      "Postoperative Antibiotics",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    "antibiotics ceased at completion of surgery ?"),
                                Row(
                                  children: [
                                    antibiotikButton(0, "Yes"),
                                    antibiotikButton(1, "No")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: DropdownButton(
                                    hint: Text("Reason Given"),
                                    icon: Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    value: reasonchoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        reasonchoose = newValue;
                                      });
                                    },
                                    items: listreason.map((valueItem) {
                                      return DropdownMenuItem(
                                        value: valueItem,
                                        child: Text(valueItem),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 20,
                                  width: double.infinity,
                                  color: Colors.black54,
                                  child: Center(
                                    child: Text(
                                      "Surgical Skin Preparation",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: DropdownButton(
                                    hint: Text("Surgical Skin Preparation"),
                                    icon: Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    value: skinchoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        skinchoose = newValue;
                                      });
                                    },
                                    items: listskin.map((valueItem) {
                                      return DropdownMenuItem(
                                        value: valueItem,
                                        child: Text(valueItem),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Appropriate skin preparation technique"),
                                Row(
                                  children: [
                                    skinButton(0, "Yes"),
                                    skinButton(1, "No")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Allowed to fully dry"),
                                Row(
                                  children: [
                                    fullyDryButton(0, "Yes"),
                                    fullyDryButton(1, "No")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 20,
                                  width: double.infinity,
                                  color: Colors.black54,
                                  child: Center(
                                    child: Text(
                                      "Surgical hand preparation",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: DropdownButton(
                                    hint: Text("Surgical hand preparation"),
                                    icon: Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    value: surgicalchoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        surgicalchoose = newValue;
                                      });
                                    },
                                    items: listsurgical.map((valueItem) {
                                      return DropdownMenuItem(
                                        value: valueItem,
                                        child: Text(valueItem),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text("Time spent on procedure",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )),
                                ),
                                InkWell(
                                  onTap: () {
                                    DatePicker.showTimePicker(context,
                                        theme: DatePickerTheme(
                                          containerHeight: 210.0,
                                        ),
                                        showTitleActions: true,
                                        onConfirm: (time) {
                                      print('confirm $time');

                                      _timeProcedure =
                                          '${time.hour} : ${time.minute} : ${time.second}';

                                      setState(() {});
                                    },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en);

                                    setState(() {});
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 50.0,
                                    margin: EdgeInsets.symmetric(horizontal: 8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                children: <Widget>[
                                                  Icon(
                                                    Icons.access_time,
                                                    size: 18.0,
                                                    color: Colors.teal,
                                                  ),
                                                  Text(
                                                    " $_timeProcedure",
                                                    style: TextStyle(
                                                        color: Colors.teal,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18.0),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        Text(
                                          "  Change",
                                          style: TextStyle(
                                              color: Colors.teal,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Appropriate hand preparation technique"),
                                Row(
                                  children: [
                                    handButton(0, "Yes"),
                                    handButton(1, "No"),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 20,
                                  width: double.infinity,
                                  color: Colors.black54,
                                  child: Center(
                                    child: Text(
                                      "Theatre traffic",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                buildHeadcontFormField(),
                                SizedBox(
                                  height: 15,
                                ),
                                buildEntriesFormField(),
                                SizedBox(
                                  height: 15,
                                ),
                                buildDoorFormField(),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 20,
                                  width: double.infinity,
                                  color: Colors.black54,
                                  child: Center(
                                    child: Text(
                                      "Drain / implant",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [buildLocationFormField()],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Drain inserted ?"),
                                Row(
                                  children: [
                                    _drainButton(0, "Open"),
                                    _drainButton(1, "Closed"),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          // SizedBox(
                          //   height: 15,
                          // ),
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    "Antibiotic given in presence of drain but no infection ?"),
                                Row(
                                  children: [
                                    antibiotikButton(0, "Yes"),
                                    antibiotikButton(1, "No")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: DropdownButton(
                                    hint: Text("Implant used"),
                                    icon: Icon(Icons.arrow_drop_down),
                                    isExpanded: true,
                                    underline: SizedBox(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                    ),
                                    value: implantchoose,
                                    onChanged: (newValue) {
                                      setState(() {
                                        implantchoose = newValue;
                                      });
                                    },
                                    items: listimplant.map((valueItem) {
                                      return DropdownMenuItem(
                                        value: valueItem,
                                        child: Text(valueItem),
                                      );
                                    }).toList(),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  DefaultButton(
                    press: () async {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        widget.pasien.shower = selectshower;
                        widget.pasien.antiMicroba = selectmicroba;
                        widget.pasien.plain = selectplain;
                        widget.pasien.cukur = valueChoose;
                        widget.pasien.theater = valueChoose_2;
                        widget.pasien.prophylaxis = selectprophylaxis;
                        widget.pasien.antiBiotik = givenchoose;
                        widget.pasien.jamStartAntibotik = _timeStart;
                        widget.pasien.redosedAntibiotik = _timeEnd;
                        widget.pasien.antibiotikCeased = selectantibiotikCeased;
                        widget.pasien.reasonGiven = reasonchoose;
                        widget.pasien.surgicalSkin = skinchoose;
                       // widget.pasien.surgicalSkin = selectskinPreparation;
                        widget.pasien.fullyDry = selectfullyDry;
                        widget.pasien.surgicalHand = surgicalchoose;
                        widget.pasien.teknikTepat = selecthand;
                        widget.pasien.jumlahKaryawanAwal = headcont;
                        widget.pasien.jumlahEntri = entries;
                        widget.pasien.jumlahOpenDoor = door;
                        widget.pasien.locationImplant = location;
                        widget.pasien.typeDrain = selectdrain;
                        widget.pasien.typeImplant = implantchoose;
                        store.dispatch(UpdatePasien(pasien: widget.pasien));
                        Navigator.of(context)
                            .popUntil((route) => route.isFirst);
                        Fluttertoast.showToast(
                            msg: "Succes",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.TOP,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      }
                    },
                    text: "Save",
                  )
                ],
              ),
            ),
          )
        ],
      )),
    );
  }

  Row showerButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: shower[btnValue],
          groupValue: selectshower,
          onChanged: (value) {
            setState(() {
              print(value);
              selectshower = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row antimicrobaButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: microba[btnValue],
          groupValue: selectmicroba,
          onChanged: (value) {
            setState(() {
              print(value);
              selectmicroba = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row plainButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: plain[btnValue],
          groupValue: selectplain,
          onChanged: (value) {
            setState(() {
              print(value);
              selectplain = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row prophylaxisButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: prophylaxis[btnValue],
          groupValue: selectprophylaxis,
          onChanged: (value) {
            setState(() {
              print(value);
              selectprophylaxis = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row antibiotikButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: antibiotikCeased[btnValue],
          groupValue: selectantibiotikCeased,
          onChanged: (value) {
            setState(() {
              print(value);
              selectantibiotikCeased = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row skinButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: skinPreparation[btnValue],
          groupValue: selectskinPreparation,
          onChanged: (value) {
            setState(() {
              print(value);
              selectskinPreparation = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row fullyDryButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: fullyDry[btnValue],
          groupValue: selectfullyDry,
          onChanged: (value) {
            setState(() {
              print(value);
              selectfullyDry = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row handButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: hand[btnValue],
          groupValue: selecthand,
          onChanged: (value) {
            setState(() {
              print(value);
              selecthand = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row handPrepareButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: handPrepare[btnValue],
          groupValue: selecthandPrepare,
          onChanged: (value) {
            setState(() {
              print(value);
              selecthandPrepare = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  Row _drainButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: drain[btnValue],
          groupValue: selectdrain,
          onChanged: (value) {
            setState(() {
              print(value);
              selectdrain = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  TextFormField buildHrdateFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => hrdate = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "HR Date",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildHeadcontFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => headcont = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Headcount at start of operation",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildEntriesFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => entries = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Number of Entries during operation",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildDoorFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => door = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Door openings during operation",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildLocationFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => location = newValue,
      // validator: (value) {
      //   if (value.isEmpty) {
      //     addError(error: kEmailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
        labelText: "Location",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }
}
