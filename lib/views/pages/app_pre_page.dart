import 'package:flutter/material.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/views/pages/pre_page.dart';
import 'package:ssimu/views/pages/pri_page.dart';

class AppPrePage extends StatefulWidget {
  final Pasien pasien;

  const AppPrePage({Key key, this.pasien}) : super(key: key);
  @override
  _AppPrePageState createState() => _AppPrePageState();
}

class _AppPrePageState extends State<AppPrePage> {
  PageController _pageController;
  int _currentPage = 0;

  Pasien p;
  @override
  void initState() {

    p = widget.pasien;
    print("cek");
    super.initState();
    _pageController = PageController(keepPage: true);
  }

  @override
  Widget build(BuildContext context) {
    var pages = <Widget>[
      PrePage(
        //jumpToPage: _jumpToPage,
        onEdit: _onEdit,
        pasien: p,
      ),
      PriPage(
        pasien: p,
      )
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text("Pre Surveillance Form",
            style: TextStyle(color: Colors.white)),
        backgroundColor: Color(0XFF28a745),
      ),
      body: PageView(
        children: pages,
        controller: _pageController,
        onPageChanged: (value) {
          setState(() {
            _currentPage = value;
            print("cek");
          });
        },
      ),
    );
  }

  

  _jumpToPage(int page, {Pasien pasien}) {
    _pageController.animateToPage(page,
        duration: Duration(milliseconds: 300), curve: Curves.linear);

    print("cek");
  }

  _onEdit(Pasien pasien){
    _jumpToPage(1, pasien: pasien);

    p = pasien;
    print("cek");
  }

  

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
}
