import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/states/pasien_action.dart';
import 'package:ssimu/views/pages/user_form_page.dart';
import 'package:ssimu/views/widgets/card_pasien.dart';

class PasienPage extends StatefulWidget {
  @override
  _PasienPageState createState() => _PasienPageState();
}

class _PasienPageState extends State<PasienPage> {
  BuildContext localContext;

  var store;

  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<AppState>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("List Patient", style: TextStyle(color: Colors.white)),
            backgroundColor: Color(0XFF28a745),
      ),
      body: StoreConnector<AppState, _DaftarPasienViewModel>(onInit: (store) {
        store.dispatch(LoadListPasien());
      }, converter: (store) {
        print("cek");
        if (store.state.pasienState is ListPasienNotLoaded ||
            store.state.pasienState is ListPasienLoading) {
          print("cek");
          return _DaftarPasienViewModel(isLoading: true, listPasien: null);
        }
        if (store.state.pasienState is ListPasienLoaded) {
          print("cek");
          return _DaftarPasienViewModel(
              isLoading: false,
              listPasien:
                  (store.state.pasienState as ListPasienLoaded).listWeight);
        }
        print("cek");
        return _DaftarPasienViewModel(isLoading: true, listPasien: null);
      }, builder: (context, listPasienVm) {
        print("cek");
        return listPasienVm.isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: listPasienVm.listPasien.length,
                itemBuilder: (BuildContext context, int index) {
                  return CardPasien(
                    p: listPasienVm.listPasien[index],
                  );
                },
              );
      }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0XFF28a745),
        child: Icon(Icons.add),
        onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context)=> UserFormPage()));},
      ),
    );
  }
}

class _DaftarPasienViewModel {
  final bool isLoading;
  final List<Pasien> listPasien;

  _DaftarPasienViewModel({this.isLoading, this.listPasien});
}
