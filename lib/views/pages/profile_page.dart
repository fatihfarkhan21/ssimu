import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:ssimu/action/user_action.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/views/views.dart';
import 'package:ssimu/views/widgets/form_error.dart';

class ProfilePage extends StatefulWidget {
  final UserProfile userProfile;

  const ProfilePage({Key key, this.userProfile}) : super(key: key);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String fullname;
  String surveillanceNumber;

  TextEditingController name = TextEditingController();
  TextEditingController number = TextEditingController();

  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }
  var store;

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<AppState>(context);
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: Text("Surveillance Form",
                style: TextStyle(color: Colors.white)),
            backgroundColor: Color(0XFF28a745),
          ),
          SliverToBoxAdapter(
            child: Image.asset("assets/images/profile.png"),
          ),
          SliverToBoxAdapter(
            child: Center(
              child: Text("Welcome, ${widget.userProfile.userName}!",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600)),
            ),
          ),
          SliverToBoxAdapter(
            child: Center(
              child: Text("Complate Your Profile",
                  style: TextStyle(
                      color: Colors.black26,
                      fontSize: 16,
                      fontWeight: FontWeight.w600)),
            ),
          ),
          SliverToBoxAdapter(
              child: Form(
                key: _formKey,
                  child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                buildfullNameFormField(),
                SizedBox(
                  height: 16,
                ),
                buildSurveillanceNumberFormField(),
                FormError(errors: errors)
              ],
            ),
          ))),
          SliverToBoxAdapter(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: DefaultButton(
              press: () {

                if (_formKey.currentState.validate()) {
                 _formKey.currentState.save();

                 widget.userProfile.fullName = fullname;
                 widget.userProfile.noPengawas = surveillanceNumber;
                 store.dispatch(UpdateProfileAction(user:  widget.userProfile));
                 print("cek");
                }
              },
              text: "Save",
            ),
          ))
        ],
      ),
    );
  }

  TextFormField buildfullNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => fullname = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Full Name",
        errorStyle: TextStyle(color: Colors.black),
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildSurveillanceNumberFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => surveillanceNumber = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kSurveillanceNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Surveillance Number",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }
}
