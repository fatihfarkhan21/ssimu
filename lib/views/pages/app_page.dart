import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/states/auth_state.dart';
import 'package:ssimu/views/pages/anlisis_page.dart';
import 'package:ssimu/views/pages/pasien_list_page.dart';
import 'package:ssimu/views/pages/pasien_page.dart';
import 'package:ssimu/views/pages/pre_page.dart';
import 'package:ssimu/views/pages/pre_post_page.dart';
import 'package:ssimu/views/pages/profile_page.dart';
import 'package:ssimu/views/widgets/pasien_list_post_page.dart';

import '../views.dart';
import 'app_pre_page.dart';

class AppPage extends StatefulWidget {
  @override
  _AppPageState createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  var store;
  BuildContext localContext;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    localContext = context;
    store = StoreProvider.of<AppState>(context);
    return StoreConnector<AppState, _UserProfileViewModel>(
      converter: (store) {
        if (!(store.state.authState is AuthenticationUnauthenticated ||
            store.state.authState is AuthenticationLoading)) {
          return _UserProfileViewModel(
              isLoading: false,
              userProfile:
                  (store.state.authState as AuthenticationAuthenticated).user);
        }
        return _UserProfileViewModel(isLoading: true, userProfile: null);
      },
      builder: (context, userVm) {
        return (userVm.userProfile.fullName == null &&
                userVm.userProfile.noPengawas == null)
            ? ProfilePage(
                userProfile: userVm.userProfile,
              )
            : Scaffold(
                appBar: AppBar(
                  title: Text("Surgical Site Infection Surveillance ",
                      style: TextStyle(color: Colors.white)),
                  backgroundColor: Color(0XFF28a745),
                  elevation: 0,
                ),
                body: SafeArea(
                    child: Stack(
                  children: [
                    Container(
                        height: size.height / 3,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage("assets/images/background.png"),
                                scale: 2.8,
                                alignment: Alignment.topCenter))),
                    Container(
                        margin: EdgeInsets.only(top: size.height / 5),
                        child: CustomScrollView(
                          slivers: <Widget>[
                            SliverToBoxAdapter(
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => PasienListPage()));
                                    },
                                    child: Container(
                                      height: size.height / 4,
                                      width: size.width / 2,
                                      child: Card(
                                        margin: EdgeInsets.all(8),
                                        elevation: 5,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Pre-operative data collection",
                                              textAlign: TextAlign.center,
                                            ),
                                            Image.asset(
                                              "assets/images/doctor_logo.png",
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PasienListPostPage()));
                                    },
                                    child: Container(
                                      height: size.height / 4,
                                      width: size.width / 2,
                                      child: Card(
                                        margin: EdgeInsets.all(8),
                                        elevation: 5,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Post-operative data collection",
                                              textAlign: TextAlign.center,
                                            ),
                                            Image.asset(
                                              "assets/images/nurse_logo.png",
                                              scale: 5,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SliverToBoxAdapter(
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PasienPage()));
                                    },
                                    child: Container(
                                      height: size.height / 4,
                                      width: size.width / 2,
                                      child: Card(
                                        margin: EdgeInsets.all(8),
                                        elevation: 5,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text("Patient List"),
                                            Image.asset(
                                              "assets/images/patient_logo.png",
                                              scale: 4,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AnalisisPage()));},
                                    child: Container(
                                      height: size.height / 4,
                                      width: size.width / 2,
                                      child: Card(
                                        margin: EdgeInsets.all(8),
                                        elevation: 5,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text("Analysis"),
                                            Image.asset(
                                                "assets/images/chart_logo.png"),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                  ],
                )),
              );
      },
    );
  }
}

class _UserProfileViewModel {
  final bool isLoading;
  final UserProfile userProfile;

  _UserProfileViewModel({this.isLoading, this.userProfile});
}
