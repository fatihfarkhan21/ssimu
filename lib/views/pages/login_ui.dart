import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ssimu/action/user_action.dart';
import 'package:ssimu/states/app_state.dart';

class LoginUi extends StatelessWidget {
  var store;
  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<AppState>(context);
    return Scaffold(
      backgroundColor: Color(0XFF28a745),
      body: SafeArea(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CircleAvatar(
                      radius: 50,
                      child: Image.asset("assets/images/umy_logo.png"),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text("Surgical site infection Surveillance",
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.bold,
                                color: Colors.white))),
                    Text("Version 1.0.0",
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                            color: Colors.white))
                  ],
                ),
              ),
              SizedBox(height: 120),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: FlatButton(
                   padding: const EdgeInsets.only(left: 16.0, top: 14, bottom: 14),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.white,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(60)),
                    onPressed: () {
                      store.dispatch(AuthWithGoogle());
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 15,
                          child: Image.asset("assets/images/login_google.png"),
                        ),
                        SizedBox(width: 20),
                        Text("Sign In With Google",
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.normal,
                                fontSize: 14,
                                color: Colors.black26)),
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
