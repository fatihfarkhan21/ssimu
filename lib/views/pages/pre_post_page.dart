import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:search_widget/search_widget.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/model/surveillance.dart';
import 'package:ssimu/model/symptoms.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/states/auth_state.dart';
import 'package:ssimu/views/widgets/search.dart';

import '../views.dart';

class PrePostPage extends StatefulWidget {
  final Pasien pasien;

  const PrePostPage({Key key, this.pasien}) : super(key: key);
  @override
  _PrePostPageState createState() => _PrePostPageState();
}

class _PrePostPageState extends State<PrePostPage> {
  DateTime _tanggalMonitor;
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  String name;

  List symptoms = ["Superficial SSI", "Deep SSI", "Organ/space SSI"];

  List superficial = [
    "cellulitis",
    "Purulent drainage (pus) fromsuperficial incision",
    "Organism identified (if culture done)",
    "Superficial incision deliberatelyre-opened",
    "Infection symptoms",
    "Surgeon/attending physiciandiagnosis"
  ];

  List deep = [
    "deep abscess",
    "Purulent drainage (pus) from deepincision",
    "Deep incision dehiscence or deliberately opened by surgeon",
    "Organism identified (if culture done)",
    "Infection symptoms",
    "Deep infection/abscess foundon imaging/examination"
  ];

  List organ = [
    "Deeper than fascia/muscle e.g. endometritis (organ), peritonitis (space)",
    "Purulent drainage (pus) from sterile organ or space (from an inserted drain)",
    "Organ or space infection/abscess found on imaging/examination",
    "Organism identified from fluid/tissue from organ/ space"
  ];

  List detail = [];

  String valueDetails;
  String valueChoose;

  String categoryAntibiotik;
  String antibiotikValue;
  List antibiotik = [];
  List antibiotikCategory = [
    'AMINOGLIKOSIDA',
    'SEFALOSPORIN',
    'PENISILIN',
    'BETALAKTAM GOLONGANLAIN',
    'KLORAMFENIKOL',
    'MAKROLID',
    'KUINOLON',
    'TETRASIKLIN',
    'KOMBINASI ANTIBAKTERIAL',
    'ANTIBIOTIK GOLONGANLAIN'
  ];

  List aminoglikosida = [
    "Amikasin Sulfat",
    "Gentamycin",
    "Streptomisin sulfat"
  ];

  List sefalosporin = [
    "Cefadroxil",
    "Cefepime",
    "Cefixime",
    "Cefoperazone",
    "Cefotaxime",
    "Cefpirome",
    "Cefprozil",
    "Ceftazidime",
    "Ceftriaxone",
    "Cefuroxime"
  ];

  Surveillance surveillance;

  @override
  void initState() {
    var p = widget.pasien;
    print("cek");
    _tanggalMonitor = DateTime.now();
    super.initState();
  }

  var store;
  BuildContext localContext;

  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<AppState>(context);
    return StoreConnector<AppState, _UserProfileViewModel>(
      converter: (store) {
        if (!(store.state.authState is AuthenticationUnauthenticated ||
            store.state.authState is AuthenticationLoading)) {
          return _UserProfileViewModel(
              isLoading: false,
              userProfile:
                  (store.state.authState as AuthenticationAuthenticated).user);
        }
        return _UserProfileViewModel(isLoading: true, userProfile: null);
      },
      builder: (context, userVm) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Post Form", style: TextStyle(color: Colors.white)),
            backgroundColor: Color(0XFF28a745),
          ),
          body: SafeArea(
              child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        colorFilter: ColorFilter.mode(
                            Colors.white.withOpacity(0.2), BlendMode.dstATop),
                        image: AssetImage("assets/images/background.png"))),
              ),
              SingleChildScrollView(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Surveillance", style: TextStyle(fontSize: 12)),
                        Text("${userVm.userProfile.fullName ?? ""}",
                            style: TextStyle(fontSize: 16)),
                      ],
                    ),
                    SizedBox(height: 10),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Form(
                                    key: _formKey,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            DateCardRow(
                                              tanggal: _tanggalMonitor,
                                            ),
                                            Icon(Icons.calendar_today_outlined)
                                          ],
                                        ),
                                        SizedBox(height: 10),
                                        buildNameFormField(),
                                        SizedBox(height: 10),
                                        Container(
                                          padding:
                                              const EdgeInsets.only(left: 8),
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.grey, width: 1),
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          child: DropdownButton(
                                            hint: Text("Antibiotic Category "),
                                            icon: Icon(Icons.arrow_drop_down),
                                            isExpanded: true,
                                            underline: SizedBox(),
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                            ),
                                            value: categoryAntibiotik,
                                            onChanged: (newValue) {
                                              setState(() {
                                                categoryAntibiotik = newValue;
                                                if (categoryAntibiotik ==
                                                    antibiotikCategory[0]) {
                                                  antibiotik = aminoglikosida;
                                                  antibiotikValue =
                                                      aminoglikosida[0];
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[1]) {
                                                  antibiotik = sefalosporin;
                                                  antibiotikValue =
                                                      sefalosporin[0];
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[2]) {
                                                  antibiotik = [
                                                    "Amoxicillin",
                                                    "Ampicilin",
                                                    "Procain penicillin",
                                                    "Piperacillin"
                                                  ];
                                                  antibiotikValue =
                                                      "Amoxicillin";
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[3]) {
                                                  antibiotik = ["Meropenem"];
                                                  antibiotikValue = "Meropenem";
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[4]) {
                                                  antibiotik = [
                                                    "Kloramfenikol",
                                                    "Tiamfenikol"
                                                  ];
                                                  antibiotikValue =
                                                      "Kloramfenikol";
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[5]) {
                                                  antibiotik = [
                                                    "Azithromycin",
                                                    "Erythromycin",
                                                    "Spiramycin"
                                                  ];
                                                  antibiotikValue =
                                                      "Azithromycin";
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[6]) {
                                                  antibiotik = [
                                                    "Ciprofloxacin",
                                                    "Levofloxacin",
                                                    "Ofloxacine"
                                                  ];
                                                  antibiotikValue =
                                                      "Ciprofloxacin";
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[7]) {
                                                  antibiotik = ["Doxicyclin"];
                                                  antibiotikValue =
                                                      "Doxicyclin";
                                                } else if (categoryAntibiotik ==
                                                    antibiotikCategory[8]) {
                                                  antibiotik = [
                                                    "Cotrimoxazole (Sulfametoxazole 400mg &Trimetoprim 80mg)"
                                                  ];
                                                  antibiotikValue =
                                                      "Cotrimoxazole";
                                                } else if (categoryAntibiotik ==
                                                    categoryAntibiotik[9]) {
                                                  antibiotik = [
                                                    "Clindamycin",
                                                    "Fosfomycin Na",
                                                    "Metronidazole",
                                                    "Vancomycin"
                                                  ];
                                                  antibiotikValue =
                                                      "Clindamycin";
                                                } else {
                                                  antibiotik = [
                                                    "Ethambutol",
                                                    "Isoniazid (INH)",
                                                    "Pirazinamide",
                                                    "Rifampicin"
                                                  ];
                                                  antibiotikValue =
                                                      "Ethambutol";
                                                }
                                                // var test = symptoms;
                                                // print("cek");
                                              });
                                            },
                                            items: antibiotikCategory
                                                .map((valueItem) {
                                              return DropdownMenuItem(
                                                value: valueItem,
                                                child: Text(valueItem),
                                              );
                                            }).toList(),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 8),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey, width: 1),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: DropdownButton(
                                          hint: Text("Antibiotic "),
                                          icon: Icon(Icons.arrow_drop_down),
                                          isExpanded: true,
                                          underline: SizedBox(),
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                          value: antibiotikValue,
                                          onChanged: (newValue) {
                                            setState(() {
                                              antibiotikValue = newValue;
                                            });
                                          },
                                          items: antibiotik.map((valueItem) {
                                            return DropdownMenuItem(
                                              value: valueItem,
                                              child: Text(valueItem),
                                            );
                                          }).toList(),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 8),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey, width: 1),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: DropdownButton(
                                          hint: Text("Symptoms "),
                                          icon: Icon(Icons.arrow_drop_down),
                                          isExpanded: true,
                                          underline: SizedBox(),
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                          value: valueChoose,
                                          onChanged: (newValue) {
                                            setState(() {
                                              valueChoose = newValue;
                                              if (valueChoose == symptoms[0]) {
                                                detail = superficial;
                                                valueDetails = superficial[0];
                                              } else if (valueChoose ==
                                                  symptoms[1]) {
                                                detail = deep;
                                                valueDetails = deep[0];
                                              } else {
                                                detail = organ;
                                                valueDetails = organ[0];
                                              }
                                              print("cek");
                                            });
                                          },
                                          items: symptoms.map((valueItem) {
                                            return DropdownMenuItem(
                                              value: valueItem,
                                              child: Text(valueItem),
                                            );
                                          }).toList(),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 8),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.grey, width: 1),
                                            borderRadius:
                                                BorderRadius.circular(5)),
                                        child: DropdownButton(
                                          hint: Text("Sympyoms Detail"),
                                          icon: Icon(Icons.arrow_drop_down),
                                          isExpanded: true,
                                          underline: SizedBox(),
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                          value: valueDetails,
                                          onChanged: (newValue) {
                                            setState(() {
                                              valueDetails = newValue;
                                            });
                                          },
                                          items: detail.map((valueItem) {
                                            return DropdownMenuItem(
                                              value: valueItem,
                                              child: Text(valueItem),
                                            );
                                          }).toList(),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 16.0),
                                        child: DefaultButton(
                                            text: "SAVE",
                                            press: () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                _formKey.currentState.save();
                                                var sym = Symptoms(
                                                  nama: "Deep SSI",
                                                  pasien: [widget.pasien.pasienId],
                                                  sympyomsid: "h4nY7WxmN1vEDDS9DDxr"
                                                );
                                                var surveillance = Surveillance(
                                                    event: name,
                                                    antibiotik: antibiotikValue,
                                                    analis: userVm
                                                        .userProfile.fullName,
                                                    gejala: valueChoose,
                                                    gejalaDetail: valueDetails,
                                                    tanggalOperasi:
                                                        _tanggalMonitor
                                                            .toString(),
                                                      petugas: userVm.userProfile.fullName
                                                    );
                                                print("cek");
                                                addSurveillance(widget.pasien,
                                                    surveillance, sym);
                                                Navigator.of(context).popUntil(
                                                    (route) => route.isFirst);
                                                Fluttertoast.showToast(
                                                    msg: "Succes",
                                                    toastLength:
                                                        Toast.LENGTH_SHORT,
                                                    gravity: ToastGravity.TOP,
                                                    timeInSecForIosWeb: 1,
                                                    backgroundColor: Colors.red,
                                                    textColor: Colors.white,
                                                    fontSize: 16.0);
                                                print("cek");
                                              }
                                            }),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
        );
      },
    );
  }

  addSurveillance(Pasien p, Surveillance s, Symptoms symptoms ) {
    List<Surveillance> list = [s];

    var test = symptoms;
    
    if (p.surveillance == null) {
      p.surveillance = list;

    
      print("cek");
    } else {
      p.surveillance.add(s);
    }
    

    store.dispatch(UpdatePasien(pasien: p, symptoms: symptoms));
    print("cek");

    
  }

  List detailSymptoms() {
    if (symptoms == symptoms[0]) {
      detail = superficial;

      return detail;
    } else if (symptoms == symptoms[1]) {
      detail = deep;
      print("cek");
      return detail;
    } else {
      detail = organ;
      return detail;
    }
  }

  TextFormField buildNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEventNullError);
          return kEventNullError;
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Event",
        hintText: "Event",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }
}

class _UserProfileViewModel {
  final bool isLoading;
  final UserProfile userProfile;

  _UserProfileViewModel({this.isLoading, this.userProfile});
}
