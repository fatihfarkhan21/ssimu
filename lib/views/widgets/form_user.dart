import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/views/widgets/form_error.dart';

import '../views.dart';

class FormUser extends StatefulWidget {
  @override
  _FormUserState createState() => _FormUserState();
}

class _FormUserState extends State<FormUser> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  String age;
  String nomorPasien;
  String diagnosa;
  DateTime _tanggalLahir;
  DateTime _dateReg;
  DateTime _tanggalOperasi;
  String name;
  String pasienNumber;
  String surgicalProcedure;
  String grade;
  String ruangan;
  String lead;
  bool female = false;
  bool male = false;
  List gender = ["Male", "Female"];
  String select;

  @override
  void initState() {
    _tanggalLahir = DateTime.now();
    _dateReg = DateTime.now();
    _tanggalOperasi = DateTime.now();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var store = StoreProvider.of<AppState>(context);
    SizeConfig().init(context);
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(8),
        child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                buildNameFormField(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                buildNomorPasienFormField(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                Container(
                  padding: const EdgeInsets.only(right: 16),
                  //height: 50,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 70,
                          child: buildAgeFormField(),
                        ),
                        SizedBox(width: SizeConfig.screenWidth * 0.08),
                        Expanded(
                            child: Row(
                          children: <Widget>[
                            addRadioButton(0, 'Male'),
                            addRadioButton(1, 'Female'),
                          ],
                        )),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 8.0),
                  height: 50,
                  child: InkWell(
                    onTap: () async {
                      DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: _tanggalLahir,
                          firstDate: DateTime(1888),
                          lastDate: DateTime.now());
                      if (picked != null && picked != _tanggalLahir) {
                        setState(() {
                          _tanggalLahir = picked.add(Duration(hours: 17));
                        });
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Date Of Birth"),
                        SizedBox(width: SizeConfig.screenWidth * 0.02),
                        DateCardRow(tanggal: _tanggalLahir),
                        SizedBox(width: SizeConfig.screenWidth * 0.02),
                        Icon(Icons.calendar_today)
                      ],
                    ),
                  ),
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                buildDiagnosaFormField(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: InkWell(
                    onTap: () async {
                      DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: _dateReg,
                          firstDate: DateTime(2020),
                          lastDate: _dateReg);
                      if (picked != null && picked != _dateReg) {
                        setState(() {
                          _dateReg = picked.add(Duration(hours: 17));
                        });
                      }
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Admission Date :",
                            style: TextStyle(fontSize: 12)),
                        Row(
                          children: [
                            Expanded(child: DateCardRow(tanggal: _dateReg)),
                            Icon(Icons.calendar_today)
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                buildProcedurFormField(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                buildGradeFormField(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                buildRoomSurgicalFormField(),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                buildLeadFormField(),
                 Container(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: InkWell(
                    onTap: () async {
                      DateTime picked = await showDatePicker(
                          context: context,
                          initialDate: _tanggalOperasi,
                          firstDate: DateTime(2020),
                          lastDate: _tanggalOperasi);
                      if (picked != null && picked != _tanggalOperasi) {
                        setState(() {
                          _tanggalOperasi = picked.add(Duration(hours: 17));
                        });
                      }
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("surgery Date :",
                            style: TextStyle(fontSize: 12)),
                        Row(
                          children: [
                            Expanded(child: DateCardRow(tanggal: _tanggalOperasi)),
                            Icon(Icons.calendar_today)
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                FormError(errors: errors),
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                DefaultButton(
                  text: "Save",
                  press: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();

                      Pasien ps = Pasien(
                        nama: name,
                        diagnosa: diagnosa,
                        grade: grade,
                        jenisKelamin: select,
                        leader: lead,
                        nomorUrut: pasienNumber,
                        prosedurOperasi: surgicalProcedure,
                        ruangan: ruangan,
                        tanggalLahir:
                            _tanggalLahir.toIso8601String().substring(0, 10),
                        tanggalAdministasi:
                            _dateReg.toIso8601String().substring(0, 10),
                        usia: age,
                        tanggalOperasi: _tanggalOperasi.toIso8601String().substring(0, 10),
                      );
                      store.dispatch(AddPasien(pasien: ps));
                      print("cek");
                    }
                    Navigator.pop(context);
                  },
                )
              ],
            )),
      ),
    );
  }

  Row addRadioButton(int btnValue, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Radio(
          activeColor: Theme.of(context).primaryColor,
          value: gender[btnValue],
          groupValue: select,
          onChanged: (value) {
            setState(() {
              print(value);
              select = value;
            });
          },
        ),
        Text(title)
      ],
    );
  }

  TextFormField buildProcedurFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => surgicalProcedure = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Surgical Procedure",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  TextFormField buildRoomSurgicalFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => ruangan = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPasienRoomError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Operating theater",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  TextFormField buildLeadFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => lead = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kLeadrError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Lead Surgeon Name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,

        ///suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
      ),
    );
  }

  TextFormField buildGradeFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => grade = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPasienGradeError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Grade",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  TextFormField buildDiagnosaFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => diagnosa = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kDiagnosaNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Primary Diagnosis",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  TextFormField buildNomorPasienFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => pasienNumber = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPasienNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Patient Number",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  TextFormField buildAgeFormField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      inputFormatters: [
        LengthLimitingTextInputFormatter(2),
      ],
      onSaved: (newValue) => age = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Age",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          contentPadding: EdgeInsets.only(left: 20)),
    );
  }

  TextFormField buildNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Patient Name",
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }
}
