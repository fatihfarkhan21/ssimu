import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateCardRow extends StatelessWidget {
  final DateTime tanggal;
  final Color colors;

  DateCardRow({Key key, @required this.tanggal, this.colors}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String locale = Localizations.localeOf(context).languageCode;
    DateFormat df = DateFormat("EEEE dd MMMM yyyy", locale);
    List<String> tanggalText = df.format(tanggal).split(" ");
    return Container(
      decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(5.0))),
      // padding: EdgeInsets.all(5),

      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("${tanggalText[0]}, ",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  height: 1.2,
                  color: colors)),
          Text(
            "${tanggalText[1]} ",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                height: 1.2,
                color: colors),
          ),
          Text("${tanggalText[2]} ",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  height: 1.2,
                  color: colors)),
          Text("${tanggalText[3]}",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  height: 1.2,
                  color: colors)),
        ],
      ),
    );
  }
}
