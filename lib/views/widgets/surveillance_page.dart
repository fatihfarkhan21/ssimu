import 'package:flutter/material.dart';
import 'package:ssimu/model/pasien.dart';

import '../views.dart';

class SurveillancePage extends StatelessWidget {
  final Pasien p;

  const SurveillancePage({Key key, this.p}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail Patient", style: TextStyle(color: Colors.white)),
        backgroundColor: Color(0XFF28a745),
      ),
      body: ListView.builder(
        itemCount: p.surveillance.length,
        itemBuilder: (BuildContext context, int index) {
          var no = index + 1;
          DateTime tempDate =
              DateTime.parse(p.surveillance[index].tanggalOperasi);
          print("cek");
          return Card(
            elevation: 5,
            child: Container(
              width: 300,
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    //margin: EdgeInsets.symmetric(horizontal: 8),
                    height: 40,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Color(0XFF28a745),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5),
                            topRight: Radius.circular(5))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Day " + no.toString(),
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 16)),
                        DateCardRow(
                          tanggal: tempDate,
                          colors: Colors.white,
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Event :",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 12)),
                          Text("${p.surveillance[index].event}" ?? "-",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                          Divider(color: Colors.black),
                          Text("Antibiotic :",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 12)),
                          Text("${p.surveillance[index].antibiotik}" ?? "-",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                          Divider(color: Colors.black),
                          Text("symptoms :",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 12)),
                          Text("${p.surveillance[index].gejala}" ?? "-",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                          Divider(color: Colors.black),
                          Text("symptoms Details:",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 12)),
                          Text("${p.surveillance[index].gejalaDetail}" ?? "-",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 16)),
                        ]),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
