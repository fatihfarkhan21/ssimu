import 'package:flutter/material.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/views/pages/detail_pasien.dart';

import '../views.dart';

class CardPasien extends StatelessWidget {
  final Pasien p;

  const CardPasien({Key key, this.p}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              Container(
                  padding: const EdgeInsets.symmetric(horizontal: 8),

                  //margin: EdgeInsets.symmetric(horizontal: 8),

                  height: 40,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Color(0XFF28a745),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(5),
                          topRight: Radius.circular(5))),
                  child: Center(
                    child: Text("${p.nama ?? "-"}".toUpperCase(),
                        style: TextStyle(color: Colors.white, fontSize: 16)),
                  )),
              Positioned(
                  right: 0,
                  top: 0,
                  bottom: 0,
                  child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetailPage(
                                      pasien: p,
                                    )));
                      },
                      child:
                          Icon(Icons.arrow_forward_ios, color: Colors.white)))
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Primary diagnosis :",
                        style: TextStyle(color: Colors.black, fontSize: 12)),
                    Text("${p.diagnosa ?? "-"}",
                        style: TextStyle(color: Colors.black, fontSize: 16))
                  ],
                ),
                Divider(color: Colors.black),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Surgical wound class :",
                        style: TextStyle(color: Colors.black, fontSize: 12)),
                    Text("${p.prosedurOperasi ?? "-"}",
                        style: TextStyle(color: Colors.black, fontSize: 16))
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
