import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:ssimu/reducer/app_reducer.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/views/pages/pre_page.dart';

import 'middleware/app_middleware.dart';
import 'views/pages/detail_pasien.dart';
import 'views/pages/pasien_list_page.dart';
import 'views/pages/pri_page.dart';
import 'views/views.dart';

final navigatorKey = GlobalKey<NavigatorState>();
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final store = Store<AppState>(appReducer,
      initialState: AppState.loading(), middleware: appMiddleware);

  runApp(MyApp(
    appStore: store,
  ));
}

class MyApp extends StatelessWidget {
  final Store<AppState> appStore;

  const MyApp({Key key, this.appStore}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: appStore,
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: theme(),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('id', 'ID'),
        ],
        navigatorKey: navigatorKey,
        initialRoute: '/',
        routes: {
          '/': (BuildContext context) => LoginPage(),
          '/home': (BuildContext context) => AppPage()
        },
      ),
    );
  }
}
