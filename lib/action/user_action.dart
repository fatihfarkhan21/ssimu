import 'package:equatable/equatable.dart';
import 'package:ssimu/views/views.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthAction extends Equatable {
  AuthAction([List props = const []]) : super(props);
}

class AuthInit extends AuthAction {
  @override
  String toString() => 'AuthInit';
}

class AuthWithGoogle extends AuthAction {
  @override
  String toString() => 'AuthWithGoogle';
}

class AuthSuccess extends AuthAction {
  final UserProfile userProfile;

  AuthSuccess({this.userProfile});
  @override
  String toString() => 'AuthSuccess: ${userProfile.userName}';
}


class LogOutAction extends AuthAction {
  @override
  String toString() => 'LogOutAction';
}
class UpdateProfileAction extends AuthAction{
  final UserProfile user;

  UpdateProfileAction({this.user});

  @override
  String toString() => 'UpdateProfile:${user.userName}';
}
class LogOut extends AuthAction {
  @override
  String toString() => 'LogOut';
}

class AuthFail extends AuthAction {
  final String error;
  AuthFail(this.error);
  @override
  String toString() => 'AuthFail';
}
