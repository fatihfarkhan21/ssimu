import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:ssimu/model/symptoms.dart';

@immutable
abstract class PasienAction extends Equatable {
  PasienAction([List props = const []]): super(props);
}

class LoadListPasien extends PasienAction {
  @override
  String toString() {
    return "LoadListPasien";
  }
}
class LoadListPasienDone extends PasienAction {
  final List<Pasien> listPasien;

  LoadListPasienDone({this.listPasien});

  @override
  String toString() {
    return "LoadListPasienDone";
  }
}
class ChangeProgressChartStartDate {
  final DateTime dateTime;

  ChangeProgressChartStartDate(this.dateTime);
}
class AddPasien extends PasienAction {

  final Pasien pasien;

  AddPasien({this.pasien});
  @override
  String toString() {
    return "AddPasien";
  }
}

class UpdatePasien extends PasienAction {

  final Pasien pasien;
  final Symptoms symptoms;

  UpdatePasien({this.pasien, this.symptoms});
  @override
  String toString() {
    return "UpdatePasien";
  }
}

class DeletePasien extends PasienAction {

  final Pasien pasien;

  DeletePasien({this.pasien});
  @override
  String toString() {
    return "DeletePasien";
  }
}