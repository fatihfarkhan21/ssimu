import 'package:redux_logging/redux_logging.dart';
import 'package:logging/logging.dart';
import 'package:ssimu/states/app_state.dart';

final westclicLogging = LoggingMiddleware<AppState>(
    logger: Logger('AppRedux')
      ..onRecord
          .where((record) => record.loggerName == 'AppRedux')
          .listen((loggingMiddlewareRecord) => print(loggingMiddlewareRecord)));
