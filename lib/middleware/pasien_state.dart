import 'package:redux/redux.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/repository/pasien_repository.dart';
import 'package:ssimu/states/app_state.dart';

Middleware<AppState> listPasienMiddleware(){
  return (Store<AppState> store, action, NextDispatcher next){
    print("List Pasien middleware : " + action.runtimeType.toString());

    next(action);

    if(action is AddPasien){
      _handleAddPasien(store, action);

    }else if(action is UpdatePasien){
      _handleUpdatePasien(store, action);
    }else if(action is DeletePasien){
      _handleDeletePasien(store, action);
    }
  };
}

_handleAddPasien(Store<AppState> store, AddPasien action) async {
  PasienRepository.instance.addPasien(action.pasien);
}
_handleUpdatePasien(Store<AppState> store, UpdatePasien action) async {
  PasienRepository.instance.updatePasien(action.pasien, action.symptoms);
}
_handleDeletePasien(Store<AppState> store, DeletePasien action) async {
  PasienRepository.instance.deletePasien(action.pasien);
}