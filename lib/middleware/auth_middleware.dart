import 'package:redux/redux.dart';
import 'package:ssimu/action/user_action.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:ssimu/views/views.dart';

Middleware<AppState> authaMiddleware() {
  return (Store<AppState> store, action, NextDispatcher next) {
    print('Auth  Middleware: ' + action.runtimeType.toString());
    next(action);

    print("cek");

    if (action is AuthInit) {
      print("cek");
      _handleAuthInit(store, action);
    } else if (action is AuthWithGoogle) {
      _handleAuthWithGoogle(store, action);
    } else if (action is LogOutAction) {
      _handleLogOut(store, action);
    } else if (action is UpdateProfileAction) {
      _handleUpdateProfile(store, action);
    }
  };
}

_handleAuthInit(Store<AppState> store, AuthInit action) async {
  
  UserProfile user = await UserRepository.instance.currentUser();
 
  if (user == null) {
    print("cek");
    store.dispatch(LogOut());
  } else {
    print("cek");
    store.dispatch(AuthSuccess(userProfile: user));
  }
}

_handleAuthWithGoogle(Store<AppState> store, AuthWithGoogle action) async {
  UserProfile user = await UserRepository.instance.loginWithGoogle();
  if(user != null){
    store.dispatch(AuthSuccess(userProfile: user));
  }
  
}

_handleUpdateProfile(Store<AppState> store, UpdateProfileAction action) async {
  UserRepository.instance.updateProfile(action.user);
}

_handleLogOut(Store<AppState> store, LogOutAction action) async {
  await UserRepository.instance.logOut();
  store.dispatch(LogOut());
}
