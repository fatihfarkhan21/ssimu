import 'package:redux_epics/redux_epics.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/repository/pasien_repository.dart';
import 'package:ssimu/states/app_state.dart';
import 'package:rxdart/rxdart.dart';

Stream<dynamic> getListPasienStream(
    Stream<dynamic> action, EpicStore<AppState> store) {
  final listPasienStream =
      Observable(action).whereType<LoadListPasien>();
  return listPasienStream.switchMap((action) {
    print("cek");
    return Observable(PasienRepository.instance.getPasient())
        .map((listPasien) => LoadListPasienDone(listPasien: listPasien));
  });
}
