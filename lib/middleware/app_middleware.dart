import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/action/user_action.dart';
import 'package:ssimu/middleware/auth_middleware.dart';
import 'package:ssimu/middleware/logging_middleware.dart';
import 'package:ssimu/middleware/pasien_state.dart';
import 'package:ssimu/states/app_state.dart';

import 'daftar_pasien_middleware.dart';

final appMiddleware = createWeightMiddleware();

List<Middleware<AppState>> createWeightMiddleware(){
  return [
    
    westclicLogging,
    EpicMiddleware(appEpics),

    // auth middleware
    TypedMiddleware<AppState, AuthInit>(authaMiddleware()),
    TypedMiddleware<AppState, AuthWithGoogle>(authaMiddleware()),
    TypedMiddleware<AppState, LogOutAction>(authaMiddleware()),
    TypedMiddleware<AppState, UpdateProfileAction>(authaMiddleware()),

    //EpicMiddleware(weightEpics),
    TypedMiddleware<AppState, AddPasien>(listPasienMiddleware()),
    TypedMiddleware<AppState, UpdatePasien>(listPasienMiddleware()),
    TypedMiddleware<AppState, DeletePasien>(listPasienMiddleware()),

  ];
}

final appEpics = combineEpics<AppState>([
  TypedEpic<AppState, LoadListPasien>(getListPasienStream),

  
  
]);