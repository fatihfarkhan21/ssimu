import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ssimu/model/pasien.dart';
import 'package:rxdart/rxdart.dart';
import 'package:ssimu/model/symptoms.dart';

class PasienRepository {
  List<Pasien> daftarMeja;
  CollectionReference pasienRepositoryCR =
      FirebaseFirestore.instance.collection("pasien");
  CollectionReference symptomsRepositoryCR =
      FirebaseFirestore.instance.collection("symptoms");

  static final PasienRepository instance = PasienRepository();

  Future<Pasien> addPasien(Pasien pasien) async {
    pasien.pasienId = pasienRepositoryCR.doc().id;
    pasienRepositoryCR
        .doc(pasien.pasienId)
        .set(pasien.toMap())
        .catchError((e) => print(e));
    return pasien;
  }

  Future<void> deletePasien(Pasien pasien) async {
    pasienRepositoryCR
        .doc(pasien.pasienId)
        .delete()
        .catchError((e) => print(e));
  }

  Future<void> updatePasien(Pasien pasien, Symptoms symptoms) async {
    
    pasienRepositoryCR
        .doc(pasien.pasienId)
        .update(pasien.toMap())
        .catchError((e) => print(e));
        print("");
  }

  Stream<List<Pasien>> getPasient() {
    Observable<List<Pasien>> allWeight = Observable(pasienRepositoryCR
        .snapshots()
        .map((QuerySnapshot qs) => qs.docs
            .map((DocumentSnapshot ds) => Pasien.fromMap(ds.data()))
            .toList()));

    print("cek");
    return allWeight;
  }
}
