import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:ssimu/views/views.dart';



class UserRepository {
  UserProfile userProfile;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  User user;
  GoogleSignInAccount googleAccount;
  GoogleSignInAuthentication googleAuth;
  final GoogleSignIn googleSignIn = GoogleSignIn();

   CollectionReference userProfileCR = FirebaseFirestore.instance.collection("UserProfile");
  static final UserRepository instance = UserRepository();

  Future<UserProfile> currentUser() async {
    userProfile = null;
    User user =  _auth.currentUser;

    print("cek");
    if (user != null) {
      userProfile = await getUserProfile(user);
    }
    return userProfile;
  }

  Future<UserProfile> loginWithGoogle() async {
    user = null;
    googleAccount = googleSignIn.currentUser;
    // if (googleAccount == null) {
    //   googleAccount = await googleSignIn.signInSilently();
    // }
    if (googleAccount == null) {
      googleAccount = await googleSignIn.signIn();
    }
    if (googleAccount != null) {
      googleAuth = await googleAccount.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
      user = (await _auth.signInWithCredential(credential)).user;
      print("tes");
      userProfile = await getUserProfile(user);
    }
    
    return userProfile;
  }
  Future<void> updateProfile(UserProfile user) async {
  userProfileCR
      .doc(user.userId)
      .update(user.toMap())
      .catchError((e) => print(e));
  }
  Future<UserProfile> getUserProfile(User firebaseUser) async {
    print("tes");
    DocumentSnapshot snapshot = await userProfileCR.doc(firebaseUser.uid).get();
    print("tes");
    if(!snapshot.exists){
      this.userProfile = UserProfile(userName: firebaseUser.displayName,email: firebaseUser.email,userId: firebaseUser.uid);
      await userProfileCR.doc(firebaseUser.uid).set(userProfile.toMap());
      print("tes");
    }else{
      this.userProfile = UserProfile.fromMap(snapshot.data());
       print("tes");
      print(snapshot.data);
      print("tes");
    }
    return this.userProfile;
  }

  Future<void> logOut() async {
    _auth.signOut();
  }
  
  
}
