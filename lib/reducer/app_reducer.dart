import 'package:ssimu/reducer/auth_reducer.dart';
import 'package:ssimu/reducer/pasien_reducer.dart';
import 'package:ssimu/states/app_state.dart';

AppState appReducer(AppState appState, action) {
  //print('Action  : ${action.runtimeType.toString()}');
  return AppState(
    authState: authReducer(appState.authState, action),
    pasienState: listPasienReducer(appState.pasienState, action)
  );
}
