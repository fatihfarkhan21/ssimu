import 'package:redux/redux.dart';
import 'package:ssimu/action/user_action.dart';
import 'package:ssimu/states/auth_state.dart';

final authReducer = combineReducers<AuthState>([
  TypedReducer<AuthState, AuthInit>(_authInitHandle),
  TypedReducer<AuthState, AuthWithGoogle>(_authWithGoogleHandle),
  TypedReducer<AuthState, AuthSuccess>(_authSuccessHandle),
  TypedReducer<AuthState, LogOut>(_logoutHandle),
  TypedReducer<AuthState, AuthFail>(_authFail),
  TypedReducer<AuthState, UpdateProfileAction>(_updateProfile),
]);


AuthState _authInitHandle(AuthState authState, AuthInit action) {
  return AuthenticationLoading();
}

AuthState _authWithGoogleHandle(AuthState authState, AuthWithGoogle action) {
  return AuthenticationLoading();
}

AuthState _authSuccessHandle(AuthState authState, AuthSuccess action) {
  return AuthenticationAuthenticated(user: action.userProfile);
}

AuthState _logoutHandle(AuthState authState, LogOut action) {
  return AuthenticationUnauthenticated();
}

AuthState _authFail(AuthState authState, AuthFail action) {
  return AuthenticationError(errorInfo: action.error);
}

AuthState _updateProfile(AuthState authState, UpdateProfileAction action){
  return AuthenticationAuthenticated(user: action.user);
}
