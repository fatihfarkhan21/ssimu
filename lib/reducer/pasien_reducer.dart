import 'package:redux/redux.dart';
import 'package:ssimu/action/pasien_action.dart';
import 'package:ssimu/states/pasien_action.dart';

final listPasienReducer = combineReducers<ListPasienState>([
    TypedReducer<ListPasienState, LoadListPasien>(_loadListWeight),
    TypedReducer<ListPasienState, LoadListPasienDone>(_updateListWeight)
]);

ListPasienState _loadListWeight(ListPasienState listWeightState, LoadListPasien action){
  print("cek");
  return ListPasienLoading();
}
ListPasienState _updateListWeight(ListPasienState listWeightState, LoadListPasienDone action){
  print("cek");
  return ListPasienLoaded(listWeight: action.listPasien);
}