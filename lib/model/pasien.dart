// To parse this JSON data, do
//
//     final pasien = pasienFromMap(jsonString);

import 'dart:convert';

import 'package:ssimu/model/surveillance.dart';

class Pasien {
    Pasien({
        this.pasienId,
        this.nama,
        this.usia,
        this.tanggalLahir,
        this.nomorUrut,
        this.jenisKelamin,
        this.diagnosa,
        this.prosedurOperasi,
        this.tanggalAdministasi,
        this.grade,
        this.ruangan,
        this.leader,
        this.tanggalOperasi,
        this.weight,
        this.height,
        this.asaClass,
        this.woundClass,
        this.startSkinKnife,
        this.endSkinKnife,
        this.urgency,
        this.kamarMandi,
        this.shower,
        this.cukur,
        this.antiMicroba,
        this.plain,
        this.theater,
        this.prophylaxis,
        this.jamStartAntibotik,
        this.redosedAntibiotik,
        this.antibiotikCeased,
        this.reasonGiven,
        this.antiBiotik,
        this.surgicalSkin,
        this.teknikTepat,
        this.fullyDry,
        this.surgicalHand,
        this.jumlahKaryawanAwal,
        this.jumlahEntri,
        this.jumlahOpenDoor,
        this.typeDrain,
        this.locationImplant,
        this.typeImplant,
        this.surveillance,
    });

    String pasienId;
    String nama;
    String usia;
    String tanggalLahir;
    String nomorUrut;
    String jenisKelamin;
    String diagnosa;
    String prosedurOperasi;
    String tanggalAdministasi;
    String grade;
    String ruangan;
    String leader;
    String tanggalOperasi;
    String weight;
    String height;
    String asaClass;
    String woundClass;
    String startSkinKnife;
    String endSkinKnife;
    String urgency;
    String kamarMandi;
    String shower;
    String cukur;
    String antiMicroba;
    String plain;
    String theater;
    String prophylaxis;
    String jamStartAntibotik;
    String redosedAntibiotik;
    String antibiotikCeased;
    String reasonGiven;
    String antiBiotik;
    String surgicalSkin;
    String teknikTepat;
    String fullyDry;
    String surgicalHand;
    String jumlahKaryawanAwal;
    String jumlahEntri;
    String jumlahOpenDoor;
    String typeDrain;
    String locationImplant;
    String typeImplant;
    List<Surveillance> surveillance;

    factory Pasien.fromJson(String str) => Pasien.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Pasien.fromMap(Map<String, dynamic> json) => Pasien(
        pasienId: json["pasien_id"] == null ? null : json["pasien_id"],
        nama: json["nama"] == null ? null : json["nama"],
        usia: json["usia"] == null ? null : json["usia"],
        tanggalLahir: json["tanggal_lahir"] == null ? null : json["tanggal_lahir"],
        nomorUrut: json["nomor_urut"] == null ? null : json["nomor_urut"],
        jenisKelamin: json["jenis_kelamin"] == null ? null : json["jenis_kelamin"],
        diagnosa: json["diagnosa"] == null ? null : json["diagnosa"],
        prosedurOperasi: json["prosedur_operasi"] == null ? null : json["prosedur_operasi"],
        tanggalAdministasi: json["tanggal_administasi"] == null ? null : json["tanggal_administasi"],
        grade: json["grade"] == null ? null : json["grade"],
        ruangan: json["ruangan"] == null ? null : json["ruangan"],
        leader: json["leader"] == null ? null : json["leader"],
        tanggalOperasi: json["tanggal_operasi"] == null ? null : json["tanggal_operasi"],
        weight: json["weight"] == null ? null : json["weight"],
        height: json["height"] == null ? null : json["height"],
        asaClass: json["asa_class"] == null ? null : json["asa_class"],
        woundClass: json["wound_class"] == null ? null : json["wound_class"],
        startSkinKnife: json["start_skin_knife"] == null ? null : json["start_skin_knife"],
        endSkinKnife: json["end_skin_knife"] == null ? null : json["end_skin_knife"],
        urgency: json["urgency"] == null ? null : json["urgency"],
        kamarMandi: json["kamar_mandi"] == null ? null : json["kamar_mandi"],
        shower: json["shower"] == null ? null : json["shower"],
        cukur: json["cukur"] == null ? null : json["cukur"],
        antiMicroba: json["anti_microba"] == null ? null : json["anti_microba"],
        plain: json["plain"] == null ? null : json["plain"],
        theater: json["theater"] == null ? null : json["theater"],
        prophylaxis: json["prophylaxis"] == null ? null : json["prophylaxis"],
        jamStartAntibotik: json["jam_start_antibotik"] == null ? null : json["jam_start_antibotik"],
        redosedAntibiotik: json["redosed_antibiotik"] == null ? null : json["redosed_antibiotik"],
        antibiotikCeased: json["antibiotik_ceased"] == null ? null : json["antibiotik_ceased"],
        reasonGiven: json["reason_given"] == null ? null : json["reason_given"],
        antiBiotik: json["anti_biotik"] == null ? null : json["anti_biotik"],
        surgicalSkin: json["surgical_skin"] == null ? null : json["surgical_skin"],
        teknikTepat: json["teknik_tepat"] == null ? null : json["teknik_tepat"],
        fullyDry: json["fully_dry"] == null ? null : json["fully_dry"],
        surgicalHand: json["surgical_hand"] == null ? null : json["surgical_hand"],
        jumlahKaryawanAwal: json["jumlah_karyawan_awal"] == null ? null : json["jumlah_karyawan_awal"],
        jumlahEntri: json["jumlah_entri"] == null ? null : json["jumlah_entri"],
        jumlahOpenDoor: json["jumlah_open_door"] == null ? null : json["jumlah_open_door"],
        typeDrain: json["type_drain"] == null ? null : json["type_drain"],
        locationImplant: json["location_implant"] == null ? null : json["location_implant"],
        typeImplant: json["type_implant"] == null ? null : json["type_implant"],
        surveillance: json["surveillance"] == null ? null : List<Surveillance>.from(json["surveillance"].map((x) => Surveillance.fromMap(x.cast<String,dynamic>()))),
    );

    Map<String, dynamic> toMap() => {
        "pasien_id": pasienId == null ? null : pasienId,
        "nama": nama == null ? null : nama,
        "usia": usia == null ? null : usia,
        "tanggal_lahir": tanggalLahir == null ? null : tanggalLahir,
        "nomor_urut": nomorUrut == null ? null : nomorUrut,
        "jenis_kelamin": jenisKelamin == null ? null : jenisKelamin,
        "diagnosa": diagnosa == null ? null : diagnosa,
        "prosedur_operasi": prosedurOperasi == null ? null : prosedurOperasi,
        "tanggal_administasi": tanggalAdministasi == null ? null : tanggalAdministasi,
        "grade": grade == null ? null : grade,
        "ruangan": ruangan == null ? null : ruangan,
        "leader": leader == null ? null : leader,
        "tanggal_operasi": tanggalOperasi == null ? null : tanggalOperasi,
        "weight": weight == null ? null : weight,
        "height": height == null ? null : height,
        "asa_class": asaClass == null ? null : asaClass,
        "wound_class": woundClass == null ? null : woundClass,
        "start_skin_knife": startSkinKnife == null ? null : startSkinKnife,
        "end_skin_knife": endSkinKnife == null ? null : endSkinKnife,
        "urgency": urgency == null ? null : urgency,
        "kamar_mandi": kamarMandi == null ? null : kamarMandi,
        "shower": shower == null ? null : shower,
        "cukur": cukur == null ? null : cukur,
        "anti_microba": antiMicroba == null ? null : antiMicroba,
        "plain": plain == null ? null : plain,
        "theater": theater == null ? null : theater,
        "prophylaxis": prophylaxis == null ? null : prophylaxis,
        "jam_start_antibotik": jamStartAntibotik == null ? null : jamStartAntibotik,
        "redosed_antibiotik": redosedAntibiotik == null ? null : redosedAntibiotik,
        "antibiotik_ceased": antibiotikCeased == null ? null : antibiotikCeased,
        "reason_given": reasonGiven == null ? null : reasonGiven,
        "anti_biotik": antiBiotik == null ? null : antiBiotik,
        "surgical_skin": surgicalSkin == null ? null : surgicalSkin,
        "teknik_tepat": teknikTepat == null ? null : teknikTepat,
        "fully_dry": fullyDry == null ? null : fullyDry,
        "surgical_hand": surgicalHand == null ? null : surgicalHand,
        "jumlah_karyawan_awal": jumlahKaryawanAwal == null ? null : jumlahKaryawanAwal,
        "jumlah_entri": jumlahEntri == null ? null : jumlahEntri,
        "jumlah_open_door": jumlahOpenDoor == null ? null : jumlahOpenDoor,
        "type_drain": typeDrain == null ? null : typeDrain,
        "location_implant": locationImplant == null ? null : locationImplant,
        "type_implant": typeImplant == null ? null : typeImplant,
        "surveillance": surveillance == null ? null : List<dynamic>.from(surveillance.map((x) => x.toMap())),
    };
}


