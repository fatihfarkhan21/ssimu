import 'dart:convert';

class UserProfile {
  String userId;
  String userName;
  String fullName;
  String email;
  String noPengawas;
  

  UserProfile({this.userId, this.userName, this.fullName, this.email, this.noPengawas});

  factory UserProfile.fromJson(String str) =>
      UserProfile.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory UserProfile.fromMap(Map<String, dynamic> json) => UserProfile(
        userId: json["userId"] == null ? null : json["userId"],
        userName: json["userName"] == null ? null : json["userName"],
        fullName: json["displayName"] == null ? null : json["displayName"],
        email: json["email"] == null ? null : json["email"],
        noPengawas: json["noPengawas"] == null ? null : json["noPengawas"],
      );

  Map<String, dynamic> toMap() => {
        "userId": userId == null ? null : userId,
        "userName": userName == null ? null : userName,
        "displayName": fullName == null ? null : fullName,
        "email": email == null ? null : email,
        "noPengawas": noPengawas == null ? null : noPengawas
      };
}
