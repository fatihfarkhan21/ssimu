import 'dart:convert';

class Surveillance {
    Surveillance({
        this.tanggalOperasi,
        this.event,
        this.antibiotik,
        this.gejala,
        this.analis,
        this.gejalaDetail,
        this.petugas,
    });

    String tanggalOperasi;
    String event;
    String antibiotik;
    String gejala;
    String analis;
    String gejalaDetail;
    String petugas;

    factory Surveillance.fromJson(String str) => Surveillance.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Surveillance.fromMap(Map<String, dynamic> json) => Surveillance(
        tanggalOperasi: json["tanggal_operasi"] == null ? null : json["tanggal_operasi"],
        event: json["event"] == null ? null : json["event"],
        antibiotik: json["antibiotik"] == null ? null : json["antibiotik"],
        gejala: json["gejala"] == null ? null : json["gejala"],
        analis: json["analis"] == null ? null : json["analis"],
        gejalaDetail: json["gejala_detail"] == null ? null : json["gejala_detail"],
        petugas: json["petugas"] == null ? null : json["petugas"],
    );

    Map<String, dynamic> toMap() => {
        "tanggal_operasi": tanggalOperasi == null ? null : tanggalOperasi,
        "event": event == null ? null : event,
        "antibiotik": antibiotik == null ? null : antibiotik,
        "gejala": gejala == null ? null : gejala,
        "analis": analis == null ? null : analis,
        "gejala_detail": gejalaDetail == null ? null : gejalaDetail,
        "petugas": petugas == null ? null : petugas,
    };
}