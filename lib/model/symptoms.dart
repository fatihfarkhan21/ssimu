import 'dart:convert';

class Symptoms {
    Symptoms({
        this.nama,
        this.sympyomsid,
        this.pasien,
    });

    String nama;
    String sympyomsid;
    List<String> pasien;

    factory Symptoms.fromJson(String str) => Symptoms.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Symptoms.fromMap(Map<String, dynamic> json) => Symptoms(
        nama: json["nama"] == null ? null : json["nama"],
        sympyomsid: json["sympyomsid"] == null ? null : json["sympyomsid"],
        pasien: json["pasien"] == null ? null : List<String>.from(json["pasien"].map((x) => x)),
    );

    Map<String, dynamic> toMap() => {
        "nama": nama == null ? null : nama,
        "sympyomsid": sympyomsid == null ? null : sympyomsid,
        "pasien": pasien == null ? null : List<dynamic>.from(pasien.map((x) => x)),
    };
}
