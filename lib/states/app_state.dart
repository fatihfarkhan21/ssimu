import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ssimu/states/pasien_action.dart';

import 'auth_state.dart';


@immutable
class AppState extends Equatable {
  final AuthState authState;
  final ListPasienState pasienState;

  AppState({this.pasienState, this.authState});

  AppState copyWith({
    AuthState authState,
    ListPasienState pasienState
  }){
    return AppState(
      authState: authState ?? this.authState,
      pasienState: pasienState ?? this.pasienState
    );
  }

  factory AppState.loading() => AppState(
    authState: AuthenticationUnauthenticated(),
    pasienState: ListPasienNotLoaded()
  );

  @override
  String toString(){
    return 'AppState {AuthState: $authState}, AppState {pasienState: $pasienState}';
  }
}