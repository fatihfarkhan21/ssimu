
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ssimu/model/pasien.dart';


@immutable
abstract class ListPasienState extends Equatable {
  ListPasienState([List props = const []]): super(props);
}

class ListPasienNotLoaded extends ListPasienState {
  @override
  String toString() {
    return "ListPasienNotLoaded";
  }
}
class ListPasienLoading extends ListPasienState {
  @override
  String toString() {
    return "ListPasienLoading";
  }
}
class ListPasienLoaded extends ListPasienState {
  final List<Pasien> listWeight;

  ListPasienLoaded({this.listWeight});
  @override
  String toString() {
    return "ListPasienLoaded";
  }
}
