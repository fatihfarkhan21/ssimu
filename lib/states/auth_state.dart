import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ssimu/views/views.dart';

@immutable
abstract class AuthState extends Equatable {
  final UserProfile user;
  AuthState(this.user, [List props = const []]) : super(props);
}

class AuthenticationUnauthenticated extends AuthState {
  AuthenticationUnauthenticated() : super(UserProfile());

  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationLoading extends AuthState {
  AuthenticationLoading() : super(UserProfile());

  @override
  String toString() => 'AuthenticationLoading';
}

class AuthenticationAuthenticated extends AuthState {
  final UserProfile user;
  AuthenticationAuthenticated({this.user}) : super(null);

  @override
  String toString() => 'AuthenticationAuthenticated {userId: ${user.userId}}';
}

class AuthenticationError extends AuthState {
  final String errorInfo;
  AuthenticationError({this.errorInfo}) : super(null);
  @override
  String toString() => 'AuthenticationError { error: $errorInfo}';
}
